package view;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import utility.MathUtils;


import model.ModelControl;
import model.exporter.Exporter;

public class ExportDialog implements ExportDialog_I {

	private Exporter exporter;
	
	private JDialog 	dialog;
	private JPanel		pane;

	
		private JLabel	lblFormat;
			private JPanel pnlFormat;
				private ButtonGroup 	grpFormat;
				private JRadioButton[] 	btsFormat;
			
		private JLabel lblYears;
			private JPanel pnlYears;
				private ButtonGroup grpYears;
					private JRadioButton	btnAll;
					private JRadioButton	btnCurrent;
					private JRadioButton	btnFromTo;
					private JTextField		txtFromTo;
			
		private JLabel lblAttributes;
		private JPanel pnlAttributes;
			private JCheckBox[] grpAttributes;
		
		private JButton 	btnExport;

	
	public ExportDialog(Exporter e) {
		
		this.exporter = e;
		
		dialog = new JDialog(dialog, "Export");
		dialog.setTitle("Export");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setResizable(false);
		dialog.setSize(new Dimension(400, 550));
		dialog.setLayout(new GridBagLayout());
		dialog.setVisible(true);
		
		pane = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
			
			
			lblFormat = new JLabel("Select Format:");
			c.gridx = 0;
			c.gridy = 0;
			pane.add(lblFormat, c);
			pnlFormat = new JPanel();
				grpFormat 	= new ButtonGroup();
				int noOfFormats = exporter.getFormatList().size();
				pnlFormat.setLayout(new GridLayout(MathUtils.roundUp(noOfFormats, 3), 3));
				btsFormat = new JRadioButton[noOfFormats+1];
				for (int i=0; i<btsFormat.length-1; i++) {
					btsFormat[i] = new JRadioButton(exporter.getFormatList().get(i).getName());		//create new RadioButton for each format
					grpFormat.add(btsFormat[i]);				//add to buttonGroup
					pnlFormat.add(btsFormat[i]);				//add to panel
				}//for
				btsFormat[noOfFormats] = new JRadioButton("All");				//create new RadioButton for all
				btsFormat[noOfFormats].setSelected(true);
				grpFormat.add(btsFormat[noOfFormats]);
				pnlFormat.add(btsFormat[noOfFormats]);
				c.gridx = 0;
				c.gridy = 1;
				c.gridwidth = 3;
				c.weighty = 0.3;
				pane.add(pnlFormat, c);
				c.gridy = 2;
				c.gridwidth = 4;
				pane.add(new JSeparator(JSeparator.HORIZONTAL), c);

		
			lblYears = new JLabel("Select Years:");
			c.weighty = 0.0;
			c.gridy = 3;
			c.gridx = 0;
			pane.add(lblYears, c);
			pnlYears	= new JPanel(new GridLayout(2, 2));
				grpYears 	= new ButtonGroup();
				btnAll 		= new JRadioButton("All");
				btnAll.setSelected(true);
				btnCurrent 	= new JRadioButton("Current year");
				btnFromTo 	= new JRadioButton("From To");
				grpYears.add(btnCurrent);
				grpYears.add(btnFromTo);
				grpYears.add(btnAll);
				txtFromTo = new JTextField();
				pnlYears.add(btnCurrent);
				pnlYears.add(btnAll);
				pnlYears.add(btnFromTo);
				pnlYears.add(txtFromTo);
			c.gridx = 0;
			c.gridy = 4;
			c.gridwidth = 3;
			c.weighty = 0.3;
			pane.add(pnlYears, c);
			c.gridy = 5;
			c.gridwidth = 4;
			pane.add(new JSeparator(JSeparator.HORIZONTAL), c);

			
			lblAttributes = new JLabel("Select Attributes:");
			c.gridy = 6;
			c.gridx = 0;
			c.weighty = 0.0;
			pane.add(lblAttributes, c);
			pnlAttributes = new JPanel();
				String[] attrLabels = ModelControl.dummyFam.getAllAttributeLabels();
				pnlAttributes.setLayout(new GridLayout(MathUtils.roundUp(attrLabels.length, 3), 3));
				String[] attrMapping = ModelControl.dummyFam.getDefaultAttrLabels();
				grpAttributes = new JCheckBox[attrLabels.length];
				for (int i=0; i<attrLabels.length; i++) {
					grpAttributes[i] = new JCheckBox(attrLabels[i]);
					for (int j=0; j<attrMapping.length; j++) {
						if ( grpAttributes[i].getText().equals(attrMapping[j]) ) {
							grpAttributes[i].setSelected(true);
						}//if
					}//for
					pnlAttributes.add(grpAttributes[i]);
				}//for
				c.gridx = 0;
				c.gridy = 7;
				c.gridwidth = 3;
				c.weighty = 0.4;
				pane.add(pnlAttributes, c);
				c.gridy = 8;
				c.gridwidth = 4;
				pane.add(new JSeparator(JSeparator.HORIZONTAL), c);

			
			btnExport = new JButton("Export");
			c.gridx = 2;
			c.gridy = 9;
			c.weightx = 0.0;
			c.weighty = 0.0;
			c.anchor = GridBagConstraints.LAST_LINE_END;
			pane.add(btnExport, c);
			
		dialog.getContentPane().add(pane);
		dialog.pack();
		
	}//Constructor
	
	@Override
	public void addBtnExportActionListener(ActionListener al) {
		btnExport.addActionListener(al);
	}//addBtnExportActionListener

	@Override
	public void dispose() {
		dialog.dispose();
	}//dispose

	@Override
	public String[] getAttributeMapping() {
		String[] attrMapping = new String[2];
		List<String> attrNames = new ArrayList<String>();
		for (int i=0; i<grpAttributes.length; i++) {
			if (grpAttributes[i].isSelected()) {
				attrNames.add(grpAttributes[i].getText());
			}//if
		}//for
		attrMapping = attrNames.toArray(attrMapping);
		return attrMapping;
	}//getAttributeMapping
	
	@Override
	public String getFormatSelection() {
		for (int i=0; i<btsFormat.length; i++) {
			if ( grpFormat.isSelected(btsFormat[i].getModel()) ) {
				return btsFormat[i].getText();
			}//if
		}//for
		return null;
	}//getFormatSelection

	@Override
	public void getYearSelection() {
		// TODO Auto-generated method stub
		
	}//getYearSelection



}//ExportDialog
