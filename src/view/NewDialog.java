package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextListener;
import java.io.File;
import java.util.Arrays;
import java.util.List;


import javax.swing.*;
import javax.swing.event.DocumentListener;


public class NewDialog implements NewDialog_I {
	
	private static final int 		FILE_LABEL_X 		= 0;
	private static final int 		FILE_LABEL_WIDTH 	= 2;
	
	private static final int 		STATUS_LABEL_X 		= FILE_LABEL_X + FILE_LABEL_WIDTH;
	private static final int 		STATUS_LABEL_WIDTH 	= 2;

	private static final int 		FILE_TEXT_WIDTH 	= FILE_LABEL_WIDTH + STATUS_LABEL_WIDTH;

	private static final int 		FILE_BUTTON_WIDTH 	= 1;
	private static final int		FILE_BUTTON_X		= FILE_TEXT_WIDTH;
	
	private static final Dimension 	LABEL_DIMENSION_MIN = new Dimension(200, 20);
	private static final Dimension	TEXT_DIMENSION_MAX  = new Dimension(325, 20);
	private static final Dimension 	BUTTON_DIMENSION_MIN = new Dimension(75, 20);
	
	
	private JDialog				dialog;
	private JTextField 			txtPreYears, txtMarriageFile, txtFamilyFile;
	private JComboBox<Integer> 	cmbStartYear;
	private JLabel				lblStartYear, lblPreYears;
	private JLabel 				lblMarriageFile, lblMarriageStatus, lblFamilyFile, lblFamilyStatus;
	private JButton 			btnCreate, btnMarriageFile, btnFamilyFile;

	
	
	public NewDialog() {
		
		dialog = new JDialog();
		dialog.setTitle("New Data Model");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//		dialog.setSize(350, 150);
		dialog.setResizable(false);
		dialog.setMinimumSize(new Dimension(420, 150));
		dialog.setVisible(true);
		dialog.setLayout(new GridBagLayout());


		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		
		//marriage file
		lblMarriageFile = new JLabel("Choose marriage file:");
		lblMarriageFile.setMinimumSize(LABEL_DIMENSION_MIN);
		lblMarriageFile.setSize(LABEL_DIMENSION_MIN);
		lblMarriageFile.setMaximumSize(LABEL_DIMENSION_MIN);
		c.gridx 	= FILE_LABEL_X;
		c.gridy 	= 0;
		c.gridwidth = FILE_LABEL_WIDTH;
		c.weightx 	= 1;
		dialog.getContentPane().add(lblMarriageFile, c);
		
		lblMarriageStatus = new JLabel();
		lblMarriageStatus.setMinimumSize(LABEL_DIMENSION_MIN);
		lblMarriageStatus.setSize(LABEL_DIMENSION_MIN);
		lblMarriageStatus.setForeground(Color.RED);
		c.gridx 	= STATUS_LABEL_X;
		c.gridwidth = STATUS_LABEL_WIDTH;
		dialog.getContentPane().add(lblMarriageStatus, c);
		
		txtMarriageFile = new JTextField("marriages.csv");
		txtMarriageFile.setMinimumSize(TEXT_DIMENSION_MAX);
		txtMarriageFile.setPreferredSize(TEXT_DIMENSION_MAX);
		txtMarriageFile.setMaximumSize(TEXT_DIMENSION_MAX);
		c.gridy = 1;
		c.gridx = 0;
		c.gridwidth = FILE_TEXT_WIDTH;
		dialog.getContentPane().add(txtMarriageFile, c);
		
		btnMarriageFile = new JButton(">");
		btnMarriageFile.setMinimumSize(BUTTON_DIMENSION_MIN);
		btnMarriageFile.setSize(BUTTON_DIMENSION_MIN);
		btnMarriageFile.setMaximumSize(BUTTON_DIMENSION_MIN);
		c.gridx = FILE_BUTTON_X;
		c.gridwidth = FILE_BUTTON_WIDTH;
		c.weightx = 0;
		dialog.getContentPane().add(btnMarriageFile, c);
				
		//family file
		lblFamilyFile = new JLabel("Choose family file:");
		lblFamilyFile.setMinimumSize(LABEL_DIMENSION_MIN);
		lblFamilyFile.setSize(LABEL_DIMENSION_MIN);
		c.gridx = FILE_LABEL_X;
		c.gridy = 2;
		c.gridwidth = FILE_LABEL_WIDTH;
		c.weightx = 1;
		dialog.getContentPane().add(lblFamilyFile, c);
		
		lblFamilyStatus = new JLabel();
		lblFamilyStatus.setMinimumSize(LABEL_DIMENSION_MIN);
		lblFamilyStatus.setForeground(Color.RED);
		c.gridx = STATUS_LABEL_X;
		c.gridwidth = STATUS_LABEL_WIDTH;
		dialog.getContentPane().add(lblFamilyStatus, c);
		
		txtFamilyFile = new JTextField("familyInfo.csv");
		txtFamilyFile.setMinimumSize(TEXT_DIMENSION_MAX);
		txtFamilyFile.setPreferredSize(TEXT_DIMENSION_MAX);
		txtFamilyFile.setMaximumSize(TEXT_DIMENSION_MAX);
		c.gridy = 3;
		c.gridx = 0;
		c.gridwidth = FILE_TEXT_WIDTH;
		dialog.getContentPane().add(txtFamilyFile, c);
		
		btnFamilyFile = new JButton(">");
		btnFamilyFile.setMinimumSize(BUTTON_DIMENSION_MIN);
		btnFamilyFile.setSize(BUTTON_DIMENSION_MIN);
		btnFamilyFile.setMaximumSize(BUTTON_DIMENSION_MIN);
		c.gridx = FILE_BUTTON_X;
		c.gridwidth = FILE_BUTTON_WIDTH;
		c.weightx = 0;
		dialog.getContentPane().add(btnFamilyFile, c);
		
		c.gridx = 0;
		c.gridy = 4;
		c.gridwidth = 5;
		dialog.getContentPane().add(new JSeparator(JSeparator.HORIZONTAL), c);
		
		//Start Year
		lblStartYear = new JLabel("Enter Start Year:");
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 3;
		dialog.getContentPane().add(lblStartYear, c);
		
		cmbStartYear = new JComboBox<Integer>();
		cmbStartYear.setMinimumSize(BUTTON_DIMENSION_MIN);
		cmbStartYear.setSize(BUTTON_DIMENSION_MIN);
		cmbStartYear.setMaximumSize(BUTTON_DIMENSION_MIN);
		c.gridwidth = 1;
		c.weightx = 0;
		c.gridx = 4;
		dialog.getContentPane().add(cmbStartYear, c);
		
		//Preceding Years
		lblPreYears  = new JLabel("No. of preceding years:");
		c.gridx = 0;
		c.gridy = 6;
		c.gridwidth = 3;
		dialog.getContentPane().add(lblPreYears, c);
		
		txtPreYears	 = new JTextField();
		txtPreYears.setMinimumSize(BUTTON_DIMENSION_MIN);
		txtPreYears.setSize(BUTTON_DIMENSION_MIN);
		txtPreYears.setMaximumSize(BUTTON_DIMENSION_MIN);
		txtPreYears.setText("50");
		c.weightx = 0;
		c.gridx = 4;
		c.gridwidth = 1;
		dialog.getContentPane().add(txtPreYears, c);
		
		c.gridx = 0;
		c.gridy = 7;
		c.gridwidth = 5;
		dialog.getContentPane().add(new JSeparator(JSeparator.HORIZONTAL), c);
		
		//Create Button
		btnCreate = new JButton("Create");
		c.anchor = GridBagConstraints.LAST_LINE_END;
		c.gridx = 3;
		c.gridy = 8;
		c.gridwidth = 2;
		dialog.getContentPane().add(btnCreate, c);
		
//		dialog.getContentPane().add(pane);
		dialog.pack();
		
		
		
		//"Private" ActionListeners
		
		btnFamilyFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser("");
				int returnValue = fc.showOpenDialog(dialog);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File chosenFile = fc.getSelectedFile();
					txtFamilyFile.setText(chosenFile.getAbsolutePath());
					dialog.repaint();
				}//if
			}
			
		});
		
		btnMarriageFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser("");
				int returnValue = fc.showOpenDialog(dialog);
				if (returnValue == JFileChooser.APPROVE_OPTION) {
					File chosenFile = fc.getSelectedFile();
					txtMarriageFile.setText(chosenFile.getAbsolutePath());
					dialog.repaint();
				}//if
			}//actionPerformed
			
		});
		
	}//Constructor

	@Override
	public void addBtnCreateActionListener(ActionListener al) {
		btnCreate.addActionListener(al);
	}//addBtnCreateActionListener
	
	@Override
	public void addTxtMarriageFileDocumentListener(DocumentListener dl) {
		txtMarriageFile.getDocument().addDocumentListener(dl);
	}//addTxtMarriageFileDocumentListener
	
	@Override
	public void addTxtFamilyFileTextListener(TextListener tl) {
//		txtFamilyFile.addActionListener(tl);
	}//addTxtFamilyFileActionListener
	
	
	@Override
	public int getStartYear() {
		return cmbStartYear.getItemAt(cmbStartYear.getSelectedIndex());
	}//getStartYear

	@Override
	public int getNoOfPrecedingYears() {
		return Integer.parseInt(txtPreYears.getText());
	}//getNoOfPrecedingYears
	
	@Override
	public String getMarriageFile() {
		return txtMarriageFile.getText();
	
	}//getMarriageFile
	
	@Override
	public String getFamilyFile() {
		return txtFamilyFile.getText();
	}//getFamilyFile
	
	@Override
	public void dispose() {
		dialog.dispose();
	}//dispose

	@Override
	public void setMarriageFileStatus(String text) {
		lblMarriageStatus.setText(text);
	}//setMarriageFileStatus

	@Override
	public void setFamilyFileStatus(String text) {
		lblFamilyStatus.setText(text);
	}//setFamilyFileStatus

	@Override
	public void updateYearsOfMarriagesModel(List<Integer> yearsOfMarriages) {
		Integer[] years = Arrays.copyOf(yearsOfMarriages.toArray(), yearsOfMarriages.toArray().length, Integer[].class);
		cmbStartYear.setModel(new DefaultComboBoxModel<Integer>(years));
		cmbStartYear.revalidate();
	}//updateModel


	
}//NewDialog
