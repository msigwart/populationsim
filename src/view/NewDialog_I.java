package view;

import java.awt.event.ActionListener;
import java.awt.event.TextListener;
import java.util.List;

import javax.swing.event.DocumentListener;

public interface NewDialog_I {
	
	void addBtnCreateActionListener(ActionListener al);
	void addTxtMarriageFileDocumentListener(DocumentListener dl);
	void addTxtFamilyFileTextListener(TextListener tl);

	
	
	
	/**
	 * @return returns the entered start year
	 */
	int getStartYear();
	
	/**
	 * @return returns the entered number of preceding years
	 */
	int getNoOfPrecedingYears();
	
	/**
	 * Disposes the dialog.
	 */
	void dispose();


	/**
	 * @return returns the file path for the marriage file
	 */
	String getMarriageFile();


	/**
	 * @return returns the file path for the family file
	 */
	String getFamilyFile();



	void setMarriageFileStatus(String string);



	void setFamilyFileStatus(String text);
	void updateYearsOfMarriagesModel(List<Integer> yearsOfMarriages);


}//NewDialog_I
