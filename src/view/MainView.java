package view;

import java.awt.*;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

import model.Family;
import model.ModelControl;


public class MainView implements MainView_I { 
	
	private final ModelControl model;
	
	private JFrame	mainFrame;

		//Control pane
		private JPanel paneControl;
			private JLabel 				lblCurrentYear;
			private JComboBox<Integer> 	cmbCurrentYear;
			private JButton 			btnExportMarriages;
	
		//Tabbed pane
		private JTabbedPane tabbedPane;
		
			//1. Tab
			private JScrollPane scrollFamPane;
				private AttributeManagerTable tblFamilies;
				
			//2. Tab
			private JScrollPane scrollMarPane;
				private AttributeManagerTable	tblMarriages;
			
			//3. Tab
			private JSplitPane splitStats;
				private JScrollPane scrollAlmostMarried;
					private AttributeManagerTable tblAlmostMarried;
				private JPanel pnlPotentials;
					private JPanel pnlPotControl;
						private JLabel lblPotentials;
						private JComboBox<String> cmbPotentials;
					private JScrollPane scrollSpouse;
						private AttributeManagerTable tblSpouse;
					private JScrollPane scrollPotentials;
						private AttributeManagerTable tblPotentials;
					
		//Button pane
		private JPanel		paneButton;
			private JButton btnNew;
			private JButton btnStartSimulation;
			private JButton btnExport;
			
		private JLabel	lblStatus;

		
	
	public MainView(ModelControl mc) {
		
		this.model = mc;
		
		//Main Frame
		mainFrame = new JFrame("PopulationSim");
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.setMinimumSize(new Dimension(400, 300));
		mainFrame.setPreferredSize(new Dimension(800, 600));
		mainFrame.getContentPane().setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill	= GridBagConstraints.BOTH;
		
		
			//Control panel
			paneControl = new JPanel();
				lblCurrentYear = new JLabel("Current year:");
				cmbCurrentYear = new JComboBox<Integer>();
				cmbCurrentYear.setModel(model.getCurrentYearComboBoxModel());
				cmbCurrentYear.setSelectedIndex(-1);
				btnExportMarriages = new JButton("Export marriages");
				btnExportMarriages.setEnabled(false);
				paneControl.add(lblCurrentYear);
				paneControl.add(cmbCurrentYear);
				paneControl.add(btnExportMarriages);
			
			c.gridx = 0;
			c.gridy = 0;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			mainFrame.getContentPane().add(paneControl, c);
				
			//Tabbed Pane
			tabbedPane = new JTabbedPane();
		
				//Family tab
				tblFamilies = new AttributeManagerTable();
				tblFamilies.setGridColor(Color.BLACK);
				tblFamilies.setShowHorizontalLines(false);
				tblFamilies.setModel(model.getFamilies());				//get table model from model object
				setAttributeManagerTableFormat(tblFamilies);			//sets the table format
				tblFamilies.setFillsViewportHeight(true);
				tblFamilies.setAutoCreateRowSorter(true);
				scrollFamPane = new JScrollPane(tblFamilies);
				tabbedPane.addTab("Families", scrollFamPane);
//				tblFamilies.removeColumn())
				
				//Marriage tab
				tblMarriages = new AttributeManagerTable();
				tblMarriages.setGridColor(Color.BLACK);
				tblMarriages.setShowHorizontalLines(false);
				tblMarriages.setModel(model.getMarriages());
				tblMarriages.setFillsViewportHeight(true);
				tblMarriages.setAutoCreateRowSorter(true);
				scrollMarPane = new JScrollPane(tblMarriages);
				tabbedPane.addTab("Marriages", scrollMarPane);
				
				//Statistics tab
				splitStats = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	
					//Almost married
					tblAlmostMarried = new AttributeManagerTable();
					tblAlmostMarried.setGridColor(Color.BLACK);
					tblAlmostMarried.setShowHorizontalLines(false);
					tblAlmostMarried.setModel(model.getAlmostMarried());
					scrollAlmostMarried = new JScrollPane(tblAlmostMarried);
					scrollAlmostMarried.setMinimumSize(new Dimension(100, 100));
					splitStats.add(scrollAlmostMarried);
					
					//Potentials
					pnlPotentials = new JPanel(new GridBagLayout());
					GridBagConstraints gc = new GridBagConstraints();
					gc.fill = GridBagConstraints.BOTH;
						pnlPotControl = new JPanel();
							lblPotentials = new JLabel("Choose spouse:");
							cmbPotentials = new JComboBox<String>();
							setPotentialsComboBoxModel(model.getPotentialsCmbModel());
							pnlPotControl.add(lblPotentials);
							pnlPotControl.add(cmbPotentials);
						gc.gridx = 0;
						gc.gridy = 0;
						gc.weightx = 1.0;
						pnlPotentials.add(pnlPotControl, gc);
						
						tblSpouse = new AttributeManagerTable();
						tblSpouse.setGridColor(Color.BLACK);
						tblSpouse.setShowHorizontalLines(false);
						tblSpouse.setModel(model.getSpouse());
						setAttributeManagerTableFormat(tblSpouse);											//sets the table format
						tblSpouse.setAutoCreateRowSorter(true);
						scrollSpouse = new JScrollPane(tblSpouse);
						scrollSpouse.setMinimumSize(new Dimension(200, 40));
						gc.gridy = 1;
						pnlPotentials.add(scrollSpouse, gc);
						
						tblPotentials = new AttributeManagerTable();
						tblPotentials.setGridColor(Color.BLACK);
						tblPotentials.setShowHorizontalLines(false);
						tblPotentials.setModel(model.getPotentials());
						tblPotentials.setMinimumSize(new Dimension(200, 200));
						setAttributeManagerTableFormat(tblPotentials);											//sets the table format
						tblPotentials.setAutoCreateRowSorter(true);
						scrollPotentials = new JScrollPane(tblPotentials, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
						gc.gridy = 2;
						gc.weighty = 1.0;
						pnlPotentials.add(scrollPotentials, gc);
						
					splitStats.add(pnlPotentials);
					
				tabbedPane.add("Statistics", splitStats);
							
			c.weightx = 1.0;
			c.weighty = 1.0;
			c.gridy = 1;
			mainFrame.getContentPane().add(tabbedPane, c);
		
			/* Button pane */
			paneButton = new JPanel();
			paneButton.setLayout(new GridLayout(3, 1, 0, 2));
			
				/* New Button */
				btnNew	= new JButton("New...");
				paneButton.add(btnNew);
				
				/* Statistics Button */
				btnExport	= new JButton("Export marriages...");
				btnExport.setEnabled(false);
				paneButton.add(btnExport);							//at the moment not needed
				
				/* Start Sim Button */
				btnStartSimulation 	= new JButton("Start Simulation");
				btnStartSimulation.setEnabled(false);
				paneButton.add(btnStartSimulation);
		
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = 0;
			c.gridheight = 2;
			c.weightx = 0.0;
			c.weighty = 0.0;
			mainFrame.getContentPane().add(paneButton, c);
		
		/* Status Label */
		lblStatus			= new JLabel("not yet started");
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.LAST_LINE_START;
		mainFrame.getContentPane().add(lblStatus, c);
		
		
		

		//mainFrame.add(pane);
		mainFrame.pack();
		//this.setVisible(true);
	
	}//Constructor
	


	
	private void setAttributeManagerTableFormat(AttributeManagerTable table) {
		
		table.getColumn(Family.ATTRIBUTE_NAME_PAGERANK_HTOW).setCellRenderer( new DefaultTableCellRenderer() {
			
			@Override
			public Component getTableCellRendererComponent(
					JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				
//				value = new DecimalFormat("#0.00000000000000000000000000000").format((Number)value);
				value = new DecimalFormat("0.#####E0").format((Number)value);
			
				return super.getTableCellRendererComponent(
						table, value, isSelected, hasFocus, row, column );
			}//getTableCellRendererComponent
			
		});
		
		table.getColumn(Family.ATTRIBUTE_NAME_PAGERANK_WTOH).setCellRenderer( new DefaultTableCellRenderer() {
			
			@Override
			public Component getTableCellRendererComponent(
					JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				
//				value = new DecimalFormat("#0.00000000000000000000000000000").format((Number)value);
				value = new DecimalFormat("0.#####E0").format((Number)value);

				
				return super.getTableCellRendererComponent(
						table, value, isSelected, hasFocus, row, column );
			}//getTableCellRendererComponent
			
		});
		
		
	}//setPotentialsTableFormat


//############### GENERAL METHODS ############################################################################

	@Override
	public void setVisible(boolean visible) {
		mainFrame.setVisible(visible);
	}//setVisible
	
	@Override
	public void setEnabled(boolean b) {
		mainFrame.setEnabled(b);
	}//setEnabled

	@Override
	public void setStatusLabel(String text) {
		lblStatus.setText(text);
	}//setStatusLabel
	
	@Override
	public void setButtonsEnabled(boolean b) {
		btnExport.setEnabled(b);
		btnStartSimulation.setEnabled(b);
	}//setButtonsEnabled

	
//############### ACTION LISTENERS ############################################################################
	
	@Override
	public void addBtnNewActionListener(ActionListener al) {
		btnNew.addActionListener(al);
	}//addBtnNewActionListner

	@Override
	public void addBtnExportActionListener(ActionListener al) {
		btnExport.addActionListener(al);
	}//addBtnExportActionListener
	
	@Override
	public void addBtnStartSimulationActionListener(ActionListener al) {
		btnStartSimulation.addActionListener(al);
	}//addBtnStartSimulationActionListener
	
	@Override
	public void addBtnNextYearActionListener(ActionListener al) {
		btnExportMarriages.addActionListener(al);
	}//addBtnExportMarriagesActionListener
	
	@Override
	public void addCmbCurrentYearActionListener(ActionListener al) {
		cmbCurrentYear.addActionListener(al);
	}//addCmbCurrentYearActionListener
	
	@Override
	public void addCmbPotentialsActionListener(ActionListener al) {
		cmbPotentials.addActionListener(al);
	}//addCmbPotentialsActionListener


	
	
//############### SET COMBO BOXES ############################################################################
	
	/**
	 * Used to activate all view components that should be activated when event occurs from
	 * current year combo box
	 */
	public void activateCurrentYearEvent() {
		btnExportMarriages.setEnabled(true);
	}//activateCurrentYearEvent
	

	private void setPotentialsComboBoxModel(DefaultComboBoxModel<String> cm) {
		cmbPotentials.setModel(cm);
		final Font f1 = cmbPotentials.getFont();
		final Font f2 = new Font("Tahoma", 0, 14);
		cmbPotentials.setRenderer(new DefaultListCellRenderer() {
			
			@Override
		    public Component getListCellRendererComponent(JList<?> list, Object value,
		            int index, boolean isSelected, boolean cellHasFocus) {
		        if (value instanceof JComponent)
		            return (JComponent) value;
		        
		        if (value != null) {
		        	boolean itemEnabled = !value.toString().startsWith("**"); 
			        super.getListCellRendererComponent(list, value, index,
			                isSelected && itemEnabled, cellHasFocus);
	
			        // Render item as disabled and with different font:
			        setEnabled(itemEnabled);
			        setFont(itemEnabled ? f1 : f2);
			    }//if

		        return this;
		    }//getListCellRendererComponent
			
		});
//		cmbPotentials.setSelectedIndex();

	}//setPotentialsComboBox

	
	
//############### GET COMBOBOX VALUES ############################################################################
	
	@Override
	public int getCmbCurrentYear() {
		return (int)cmbCurrentYear.getSelectedItem();
	}//getCmbCurrentYear
	
	@Override
	public String getCmbPotentialsItem() {
		return (String)cmbPotentials.getSelectedItem();
	}//getCmbPotentialsItem


	public void setPotentialsCmbIndex(int i) {
		cmbPotentials.setSelectedIndex(i);
	}//setPotentialsCmbIndex




	@Override
	public void addFamiliesAttrCheckBoxActionListener(ActionListener al) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void showFamColumn(String attrName) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void hideFamColumn(String attrName) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void addPotentialsAttrCheckBoxActionListener(ActionListener al) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public AttributeManagerTable getFamTable() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public AttributeManagerTable getPotTable() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public AttributeManagerTable getSpouseTable() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public AttributeManagerTable getMarriageTable() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public void addMarriagesAttrCheckBoxActionListener(ActionListener al) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void addBtnPrevYearActionListener(ActionListener al) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void activateComponents(int eventStartYear) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void setCmbCurrentYear(int year) {
		// TODO Auto-generated method stub
		
	}


	

}//View
