package view;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

import model.attributes.AbstractAttributeManager;

public class AttributePanel extends JPanel {
	
	private String[] allLabels;
	private String[] defaultLabels;
	
	private JCheckBox[] grpAttributes;
	
	public AttributePanel(LayoutManager lm, AbstractAttributeManager a) {
		super(lm);
		allLabels = a.getAllAttributeLabels();
		defaultLabels = a.getDefaultAttrLabels();
		
		grpAttributes = new JCheckBox[allLabels.length];
		
		Dimension buttonSize = new Dimension(140, 15);
		for (int i=0; i<allLabels.length; i++) {
			grpAttributes[i] = new JCheckBox(allLabels[i]);
			for (int j=0; j<defaultLabels.length; j++) {
				if ( grpAttributes[i].getText().equals(defaultLabels[j]) ) {
					grpAttributes[i].setSelected(true);
				}//if
			}//for
			grpAttributes[i].setPreferredSize(buttonSize);
			this.add(grpAttributes[i]);
			this.revalidate();
		}//for
	}//Constructor
	
	
	public void addActionListener(ActionListener al) {
		for (int i=0; i<grpAttributes.length; i++) {
			grpAttributes[i].addActionListener(al);
		}//for
	}//addActionListener
	
}//AttributePanel
