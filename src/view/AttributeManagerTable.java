package view;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import model.AttributeManagerTableModel;

public class AttributeManagerTable extends JTable {
	
	private TableColumnModel tcm;
	private Map<String, Object> hiddenColumns;
	
	public AttributeManagerTable(AttributeManagerTableModel<?> model) {
		super(model);
		tcm = this.getColumnModel();
		hiddenColumns = new HashMap<String, Object>();
		
	}//Constructor
	
	public AttributeManagerTable() {
		super();
		tcm = this.getColumnModel();
		hiddenColumns = new HashMap<String, Object>();
	}//Constructor
	
	
	public TableColumn getColumn(String attrName) {
		return tcm.getColumn(tcm.getColumnIndex(attrName));
	}//getColumn
	
	public void hideColumn(String attrName) {
		int index = tcm.getColumnIndex(attrName);
		TableColumn column = tcm.getColumn(index);
		hiddenColumns.put(attrName, column);
		hiddenColumns.put(":"+attrName, new Integer(index));
		tcm.removeColumn(column);
	}//hideColumn
	
	public void hideAllColumns() {
		for (int i=getColumnCount()-1; i>=0; i--) {
			hideColumn((String)tcm.getColumn(i).getIdentifier());
		}//for
	}//hideAllColumns
	
	public void showColumn(String attrName) {
		Object o = hiddenColumns.remove(attrName);
		if (o == null) {
			return;
		}//if
		tcm.addColumn((TableColumn) o);
		o = hiddenColumns.remove(":"+attrName);
		if (o == null) {
			return;
		}//if
		int column = ((Integer) o).intValue();
		int lastColumn = tcm.getColumnCount() - 1;
		if (column < lastColumn) {
			tcm.moveColumn(lastColumn, column);
		}//if
	}//showColumn
	
	
	public void showColumns(String[] attrLabels) {
		hideAllColumns();
		for (int i=0; i<attrLabels.length; i++) {
			showColumn(attrLabels[i]);
		}//for
	}//showColumns

	public boolean hasColumn(String attrName) {
		try {
			tcm.getColumnIndex(attrName);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}//catch
	}//hasColumn
	
	
}//AttributeManagerTable
