package view;

import java.awt.event.ActionListener;


public interface MainView_I {

	
	//General methods
	void setStatusLabel(String text);
	void setButtonsEnabled(boolean b);
	void setVisible(boolean visible);
	void setEnabled(boolean b);
	
	void showFamColumn(String attrName);
	void hideFamColumn(String attrName);
	
	
	//Action Listeners
	void addBtnStartSimulationActionListener(ActionListener al);
	void addBtnNewActionListener(ActionListener al);
	void addBtnPrevYearActionListener(ActionListener al);
	void addBtnNextYearActionListener(ActionListener al);
	void addBtnExportActionListener(ActionListener al);
	void addCmbCurrentYearActionListener(ActionListener al);
	void addCmbPotentialsActionListener(ActionListener al);
	
	void addFamiliesAttrCheckBoxActionListener(ActionListener al);
	void addPotentialsAttrCheckBoxActionListener(ActionListener al);
	void addMarriagesAttrCheckBoxActionListener(ActionListener al);


	
	//Set ComboBoxes
	void activateCurrentYearEvent();
	void activateComponents(int eventStartYear);

	
	//Get ComboBox values
	int getCmbCurrentYear();
	void setCmbCurrentYear(int year);
	String getCmbPotentialsItem();
	
	AttributeManagerTable getFamTable();
	AttributeManagerTable getPotTable();
	AttributeManagerTable getSpouseTable();
	AttributeManagerTable getMarriageTable();



	
}//View_I
