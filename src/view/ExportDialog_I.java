package view;

import java.awt.event.ActionListener;

public interface ExportDialog_I {
	
	void addBtnExportActionListener(ActionListener al);
	


	
	/**
	 * Disposes the dialog.
	 */
	void dispose();

	/**
	 * @return returns an array of names for all attributes to be displayed in the export.
	 */
	String[] getAttributeMapping();
	
	String getFormatSelection();
	
	void getYearSelection();
	
	
}//ExportView
