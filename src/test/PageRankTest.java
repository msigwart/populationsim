package test;

import utility.PageRank;
import Jama.Matrix;

public class PageRankTest {

	static double[][] net1 = { 	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
	
	static double[][] net2 = { 	{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0},
								{0, 0, 0, 0, 0, 0, 0, 0, 0}};
	
	static double[][] net3 = { 	{0, 0, 0, 0},
								{0, 0, 0, 0},
								{0, 0, 0, 0},
								{0, 0, 0, 0}};
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		double[][] net = net3;
		Matrix     pr  = new Matrix(initPageRankVector(net), 1);
		printNetAndPageRank(net, pr.getArray());
		
		newConnection(net, 0, 0);
		newConnection(net, 1, 1);
		newConnection(net, 2, 2);
		newConnection(net, 3, 3);

		newConnection(net, 0, 2);
//		newConnection(net, 0, 2);
//		newConnection(net, 1, 0);
//		newConnection(net, 2, 0);
//		newConnection(net, 2, 1);
//		newConnection(net, 2, 0);
//		newConnection(net, 2, 3);
//		newConnection(net, 2, 0);
//		newConnection(net, 3, 2);
//		newConnection(net, 3, 1);
		
		if (net.length > 4) {
			newConnection(net, 1, 5);
			newConnection(net, 2, 8);
			newConnection(net, 2, 8);
			newConnection(net, 2, 5);
			newConnection(net, 2, 6);
			newConnection(net, 2, 7);
			newConnection(net, 2, 8);
			newConnection(net, 2, 4);
			newConnection(net, 0, 8);
			newConnection(net, 1, 7);
			newConnection(net, 7, 5);
			newConnection(net, 7, 6);
		}


		
		printNet(net);
		pr = PageRank.calculatePageRankMatrix(pr, PageRank.calculateWeights(net)/*.transpose()*/);
		printNetAndPageRank(net, pr.getArray());
		
	}//main
	
	
	
	private static double[] initPageRankVector(double[][] net) {
		double[] pr = new double[net.length];
		for (int i=0; i<pr.length; i++) {
			pr[i] = 1.0/pr.length;
		}//for
		return pr;
	}//initPageRankVector



	static void printNetAndPageRank(double[][] net, double[][] pr) {
		for (int i=0; i<net.length; i++) {
			for (int j=0; j<net.length; j++) {
				System.out.printf("%3.0f ", net[i][j]);
			}//for
			System.out.printf("|   %.5f\n", pr[0][i]);
		}//for
		System.out.printf("\n");
	}//printNet
	
	
	static void printNet(double[][] net) {
		for (int i=0; i<net.length; i++) {
			for (int j=0; j<net.length; j++) {
				System.out.printf("%4.0f ", net[i][j]);
			}//for
			System.out.printf("\n");
		}//for
		System.out.printf("\n");
	}//printNet
	
//	static void printPageRank(double[] pr) {
//		for (int i=0; i<pr.length; i++) {
//			System.out.printf("%.2f ", pr[i]);
//		}//for
//		System.out.printf("\n\n");
//	}//printPageRank
	
	
	static void newConnection(double[][] net, int fam1, int fam2) {
		net[fam1][fam2]++;
//		net[fam2][fam1]++;
	}//newConnection

}
