package test;

import model.ModelControl;
import control.Controller;
import view.MainView;
import view.MainViewAdvanced;
import view.MainView_I;

@SuppressWarnings("unused")
public class PopulationSim {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	    System.out.println(System.getProperty("java.runtime.version"));
		
		ModelControl model = new ModelControl();
		MainView_I view = new MainViewAdvanced(model);
//		MainView_I view = new MainView(model);
		view.setVisible(true);
		
		Controller controller = new Controller(view, model);
	}

}
