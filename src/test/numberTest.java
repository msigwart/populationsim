package test;

import java.util.Arrays;

import utility.ArrayUtils;

public class numberTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
//		Integer i = 3;
//		System.out.printf("%d\n", i);
//		
//		double d = 1.0;
//		while (d > 0.0) {
//			d= d/10;
//			System.out.printf("d = %6.21f\n", d);
//		}//while
		
		String start = "Family.Person.Marriage";
		
		String[] parts = start.split("\\.");
		
		for (int i=0; i<parts.length; i++) {
			System.out.printf("%s\n", parts[i]);
		}//for
		System.out.println();
		
		parts = Arrays.copyOfRange(parts, 1, parts.length);
		
		for (int i=0; i<parts.length; i++) {
			System.out.printf("%s\n", parts[i]);
		}//for
		System.out.println();
		
		String end = ArrayUtils.join(parts, '.');
		
		System.out.printf("%s\n", end);
		
	}

}
