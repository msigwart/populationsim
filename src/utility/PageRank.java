package utility;


import Jama.Matrix;

public class PageRank {

	
//############### STATIC METHODS ############################################################################
		
	/**
	 * Calculates the weights for a pageRank calculation. It counts the total sum of connections for one row/column,
	 * and calculates a weight by dividing the number of connections by the sum.
	 * @param net the social net used for the weights calculation
	 * @return returns a Matrix object with the new weights.
	 */
	public static Matrix calculateWeights(double[][] net) {
		double[][] prWeights = new double[net.length][net.length];		//pageRank weights
		
		for (int x=0; x<prWeights.length; x++) {
			int sum = sumOutgoingConnections(net[x]);
			if (sum>0) {
				for (int y=0; y<prWeights.length; y++) {
					prWeights[x][y] = net[x][y]/sum;
				}//for
			}//if
		}//for
		return new Matrix(prWeights);
		
	}//getPageRankMatrix
	
	
	/**
	 * This recursive method calculates the new page ranks for a page ranks vector and weights Matrix derived from a social net.
	 * When there is not much difference between the preceding and the new page rank vector ( ||v1-v2|| ) the calculation is stopped.
	 * @param pageRanks the page ranks vector
	 * @param weights the weights matrix
	 * @return returns a one dimensional Matrix object with the new page ranks
	 */
	public static Matrix calculatePageRankMatrix(Matrix pageRanks, Matrix weights) {
		Matrix newPageRanks = pageRanks.times(weights);
//		printPageRanks(newPageRanks);
		double check = (pageRanks.minus(newPageRanks)).norm1();
		if (check < 0.00001) {
			System.out.printf("Norm = %.10f, Sum of page ranks: %.20f\n", check, getSumOfPageRanks(newPageRanks));
			return newPageRanks;
		} else {
//			System.out.printf("Norm = %.10f\n", check);
			return calculatePageRankMatrix(newPageRanks, weights);		//recursive call
		}//if
	}//calculatePageRank
	
	
	/**
	 * Calculates an initial page rank vector based on a given social net.
	 * @param net the social net to initialize the page rank for
	 * @return returns the initialized page rank vector.
	 */
	public static Matrix getInitPageRank(Matrix net) {
		int sum = 0;											//count of indexes with >0 connections
		for (int i=0; i<net.getRowDimension(); i++) {
			if (hasConnections(net, i)) {
				sum += 1;
			}//if
		}//for
 		double initPR 			= (double)1/sum;
		System.out.printf("Number of families with connections: %3d --> initial PageRank: %.5f\n", sum, initPR);

		double[] initPageRank	= new double[net.getRowDimension()];

		for (int i=0; i<initPageRank.length; i++) {
			if (hasConnections(net, i)) {
				initPageRank[i] = initPR;
			} else {
				initPageRank[i] = 0;
			}//if
		}//for
		return new Matrix(initPageRank, 1);
	}//initPageRank
	
	private static int sumOutgoingConnections(double[] row) {
		int sum = 0;
		for (int i=0; i<row.length; i++) {
			sum += row[i];
		}//for
		return sum;
	}//getSumOfConnections
	
	private static boolean hasConnections(Matrix net, int index) {
		if (net.get(index, index) == 0) {
			return false;
		} else {
			return true;
		}//if
	}//hasConnections
	

	
	public static void printPageRanks(Matrix pr) {
		for (int i=0; i<pr.getColumnDimension(); i++) {
			System.out.printf("%3d: %.5f\n", i, pr.get(0, i));
		}//for
	}//printPageRanks
	
	private static Double getSumOfPageRanks(Matrix pageRanks) {
		double sum = 0.0;
		for (int i=0; i<pageRanks.getColumnDimension(); i++) {
			sum += pageRanks.get(0, i);
		}//for
		return new Double(sum);
	}//getSumOfPageRanks

	
}//PageRank
