package utility;

public class MathUtils {
	
	/**
	 * Rounds up a division between two numbers, only if numbers are positive
	 * @param num
	 * @param divisor
	 * @return
	 */
	public static int roundUp(int num, int divisor) {
	    return (num + divisor - 1) / divisor;
	}//roundUp
	
}
