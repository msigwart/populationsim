package utility;


public class ArrayUtils {
	
	

	public static String[] concat(String[] a, String[] b) {
		   int aLen = a.length;
		   int bLen = b.length;
		   String[] c = new String[a.length + b.length];
		   System.arraycopy(a, 0, c, 0, aLen);
		   System.arraycopy(b, 0, c, aLen, bLen);
		   return c;
	}//concat
	
	public static String join(String[] array, char delimiter) {
		String string = "";
		for (int i=0; i<array.length; i++) {
			if (i!=0) {
				string = string + delimiter;
			}//if
			string = string + array[i];
		}//for
		return string;
	}//join
	
	
}//ArrayUtils
