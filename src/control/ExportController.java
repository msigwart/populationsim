package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;


import view.ExportDialog;
import view.ExportDialog_I;
import model.exporter.AbstractExportFormat;
import model.exporter.Exporter;


public class ExportController {
	
	private ExportDialog_I 	view;
	private Exporter		exporter;
	
	public ExportController(Exporter e) {
		this.view 		= new ExportDialog(e);
		this.exporter 	= e;
		
		view.addBtnExportActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String[] attrMapping = view.getAttributeMapping();
					
					System.out.printf("Selected Format: %s\n", view.getFormatSelection());

					if (exporter.hasFormat(view.getFormatSelection())) {		//check if passed format exists
						exporter.exportAllMarriages(view.getFormatSelection(), attrMapping, true);

					} else if (view.getFormatSelection().equalsIgnoreCase("all")){
						for (AbstractExportFormat f: exporter.getFormatList()) {
							exporter.exportAllMarriages(f, attrMapping, true);
						}//for
					}//else


				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}//for
				view.dispose();
			}//actionPerformed
			
		});
		
	}//Constructor
}//ExportController
