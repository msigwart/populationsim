package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;


import model.*;
import model.exporter.Exporter;

import view.*;

public class Controller {
	
	public static final int EVENT_START_YEAR 	= 0;
	public static final int EVENT_LAST_YEAR 	= 1;
	public static final int EVENT_BETWEEN_YEAR 	= 2;

	
	
	private MainView_I 		view;
	private ModelControl 	model;
	private Exporter		exporter;
	
	public Controller(final MainView_I view, final ModelControl mc) {
		this.view 	= view;		
		this.model 	= mc;
		exporter 	= new Exporter(model);
		
		
		//Add ActionListener for 'New' button
		this.view.addBtnNewActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				model.init();
				view.setEnabled(false);
				final NewDialog_I newDialog = new NewDialog(/*model.getYearsOfMarriages()*/);
				model.parseBasicInfo(newDialog.getMarriageFile());
				newDialog.updateYearsOfMarriagesModel(model.getYearsOfMarriages());
				
				newDialog.addTxtMarriageFileDocumentListener(new DocumentListener() {
					
					@Override
					public void removeUpdate(DocumentEvent e) {
						warn();
					}//removeUpdate
					
					@Override
					public void insertUpdate(DocumentEvent e) {
						warn();
					}//insertUpdate

					@Override
					public void changedUpdate(DocumentEvent e) {
						warn();
					}//changedUpdate
					
					
					private void warn() {
						String marriageFile = newDialog.getMarriageFile();		//get file path for marriage file
						if (marriageFile.equalsIgnoreCase("")) {
							newDialog.setMarriageFileStatus("Please Enter File Path");
							return;
						} else if ( !model.isValidMarriageFile(marriageFile) ) {
							newDialog.setMarriageFileStatus("File Path Invalid");
							return;
						} else {
							newDialog.setMarriageFileStatus("");
							model.parseBasicInfo(marriageFile);
							newDialog.updateYearsOfMarriagesModel(model.getYearsOfMarriages());
							
						}//if
					}//warn
				});
				
				
				newDialog.addBtnCreateActionListener(new ActionListener() {		//Action Listener for Create Button in Dialog

					@Override
					public void actionPerformed(ActionEvent e) {
						int startYear = newDialog.getStartYear();				//get start year to build data model from
						int precYears = newDialog.getNoOfPrecedingYears();		//get number of preceding years to build data model
						model.setParseOptions(startYear, precYears);
						
						String marriageFile = newDialog.getMarriageFile();		//get file path for marriage file
						if (marriageFile.equalsIgnoreCase("")) {
							newDialog.setMarriageFileStatus("Please Enter File Path");
							return;
						} else if ( !model.isValidMarriageFile(marriageFile) ) {
							newDialog.setMarriageFileStatus("File Path Invalid");
							return;
						} else {
							newDialog.setMarriageFileStatus("");
						}//if
						
						String familyFile = newDialog.getFamilyFile();			//get file path for family file
						if (familyFile.equalsIgnoreCase("")) {
							newDialog.setFamilyFileStatus("Please Enter File Path");
							return;
						} else if ( !model.isValidFamilyFile(familyFile) ) {
							newDialog.setFamilyFileStatus("File Path Invalid");
							return;
						} else {
							newDialog.setFamilyFileStatus("");
						}//if
						
						model.setMarriageFile(marriageFile);
						model.setFamilyFile(familyFile);
						
						
						newDialog.dispose();
						
						model.parseMarriages();									//parse marriages
						model.parseFamilyInfo();								//parse family info
						
						model.updateCurrentYearCmbModel();
						setApplicationToYear(startYear);
						view.activateComponents(EVENT_START_YEAR);
						
						exporter.writeMatrixToFile("output/socialnet.txt", true);		//true: print husbandToWife
						
						
						//Add ActionListener for 'CurrentYear' ComboBox
						view.addCmbCurrentYearActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								view.activateCurrentYearEvent();
								int year = view.getCmbCurrentYear();
								System.out.printf("Currently selected year: %d\n", year);
								
								setApplicationToYear(year);

								view.setStatusLabel("Parsed marriages until " + year);
								
							}//actionPerformed
							
						});
						
						
						//Add ActionListener for 'Potentials' ComboBox
						view.addCmbPotentialsActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								model.updatePotentialTables();
							}//actionPerformed
							
						});
						
						view.setButtonsEnabled(true);
						view.setEnabled(true);
						view.setStatusLabel(String.format("Parsed marriages until year: %d", model.getCurrentYear()));
					}//actionPerformed
					
				});
			}//actionPerformed
			
		});
		
		
		//Add ActionListener for 'Export' button
		this.view.addBtnExportActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new ExportController(exporter);
			}//actionPerformed
			
		});
		
		
		//Add ActionListener for 'StartSimulation' button
		this.view.addBtnStartSimulationActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				view.setStatusLabel("now started...");
			}//actionPerformed
			
		});
		
		
		//Add ActionListener for 'Previous Year' button
		this.view.addBtnPrevYearActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setApplicationToYear(model.getCurrentYear()-1);
			}//actionPerformed
			
		});
		
		//Add ActionListener for 'Next Year' button
		this.view.addBtnNextYearActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setApplicationToYear(model.getCurrentYear()+1);
			}//actionPerformed
			
		});
		
		this.view.addFamiliesAttrCheckBoxActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() instanceof JCheckBox) {
					JCheckBox cb = (JCheckBox)e.getSource();
					if (cb.isSelected()) {
						view.getFamTable().showColumn(cb.getText());
					} else {
						view.getFamTable().hideColumn(cb.getText());
					}//if
				}//if
			}//actionPerformed
			
		});
		
		
		this.view.addPotentialsAttrCheckBoxActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() instanceof JCheckBox) {
					JCheckBox cb = (JCheckBox)e.getSource();
					if (cb.isSelected()) {
						view.getPotTable().showColumn(cb.getText());
						view.getSpouseTable().showColumn(cb.getText());
					} else {
						view.getPotTable().hideColumn(cb.getText());
						view.getSpouseTable().hideColumn(cb.getText());

					}//if
				}//if
			}//actionPerformed
			
		});
		
		this.view.addMarriagesAttrCheckBoxActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() instanceof JCheckBox) {
					JCheckBox cb = (JCheckBox)e.getSource();
					if (cb.isSelected()) {
						view.getMarriageTable().showColumn(cb.getText());
					} else {
						view.getMarriageTable().hideColumn(cb.getText());
					}//if
				}//if
			}//actionPerformed
			
		});
		
	}//Constructor
	
	
	
	
	private void setApplicationToYear(int year) {
		model.setModelToYear(year);
		view.setCmbCurrentYear(year);
		
		if (year == model.getStartYear()) {
			view.activateComponents(EVENT_START_YEAR);
		} else if (year == model.getLastYear()) {
			view.activateComponents(EVENT_LAST_YEAR);
		} else {
			view.activateComponents(EVENT_BETWEEN_YEAR);
		}//if
	}//setApplicationToYear
	
	
	
}//Controller
