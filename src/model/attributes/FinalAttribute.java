package model.attributes;

import java.util.Observable;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;

public class FinalAttribute<E> extends AbstractAttribute<E> {

	public FinalAttribute(String name, E value) {
		super(name, value);
	}//Constructor
	
	public FinalAttribute(String name) {
		super(name);
	}//Constructor

	@Override
	public void setValue(E o) {
		if (getValue() == null) {
			super.setValue(o);
		}//if
	}////setValue

	
	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {		//value should not be modified, because attribute is final
	}


	@Override
	public void update(Observable o, Object arg) {
	}
	
}//FinalFamilyAttribute
