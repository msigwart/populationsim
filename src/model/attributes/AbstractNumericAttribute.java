package model.attributes;

public abstract class AbstractNumericAttribute<T extends Number> extends AbstractAttribute<T> implements Numerizable {

	public AbstractNumericAttribute(String name, T value) {
		super(name, value);
	}//Constructor

	public AbstractNumericAttribute(String name) {
		super(name);
	}//Constructor


	@Override
	public Number getNumValue() {
		return getValue();
	}//getNumValue




}//AbstractNumericAttribute