package model.attributes;

import java.util.Observable;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;

public class EnumAttribute<T> extends AbstractAttribute<EnumAttribute_I> implements Numerizable {

	public EnumAttribute(String name, EnumAttribute_I value) {
		super(name, value);
	}//Constructor

	@Override
	public Number getNumValue() {
		return getValue().getNumValue();
	}//getNumValue

	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {
	}

	@Override
	public void update(Observable o, Object arg) {
	}

}//EnumAttribute
