package model.attributes;

import java.util.Observable;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;

public class NumericAttribute<T extends Number> extends AbstractNumericAttribute<T> {

	public NumericAttribute(String name, T value) {
		super(name, value);
	}//Constructor
	
	public NumericAttribute(String name) {
		super(name);
	}//Constructor

	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {
	}

	@Override
	public void update(Observable o, Object arg) {
	}//update



}//NumericAttribute
