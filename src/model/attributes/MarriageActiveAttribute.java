package model.attributes;

import java.util.Observable;

import model.Marriage;


public class MarriageActiveAttribute extends BooleanAttribute {

	public MarriageActiveAttribute(String name, Boolean value, Observable o) {
		super(name, value);
		o.addObserver(this);
	}//Constructor
	
	
	@Override
	public void update(Observable o, Object arg) {
		super.update(o, arg);
		switch ((int) arg) {
			case Marriage.EVENT_MARRIAGE_STARTED:
				setValue(true);
				break;
			case Marriage.EVENT_MARRIAGE_ENDED:
				setValue(false);
				break;
			default:
				break;
		}//switch
	}//update
	
	

}//MarriageActiveAttribute
