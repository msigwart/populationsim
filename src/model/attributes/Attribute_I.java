package model.attributes;

import java.util.Observer;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;


public interface Attribute_I<T> extends Observer {
	
	/**
	 * This method should be overridden, when the recalculation of the specific attribute
	 * is done in a global manner. If the attribute does not change this should not be implemented.
	 * 
	 * @param families All families already included in the data set. By the end of the calculation, 
	 * the attribute should be updated for all
	 * 
	 * @param socialNet the social net instance used to make the calculation on.
	 */
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families);
	

	/**
	 * {@inheritDoc}
	 * <p>
	 * <B>Important:</B>
	 * This method should be overridden, when the recalculation of the attribute 
	 * is done after receiving specific events and can be handled with the scope of the attribute. 
	 * If the attribute does not change this should not be implemented.
	 */
	@Override
	public void update(java.util.Observable o, Object arg);
	
	/**
	 * Getter method for the attribute name of the attribute.
	 * @return A {@code String} with the attribute's name.
	 */
	public String getAttributeName();
	
	/**
	 * Getter method for the value of the attribute.
	 * @return The value of the attribute.
	 */
	public T getValue();
	
	/**
	 * Setter method for the value of the attribute.
	 * @param v the new value of the attribute.
	 */
	public void setValue(T v);
	
}//Attribute_I
