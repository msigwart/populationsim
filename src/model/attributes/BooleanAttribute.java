package model.attributes;

import java.util.Observable;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;

public class BooleanAttribute extends AbstractAttribute<Boolean> implements Numerizable {

	public BooleanAttribute(String name, Boolean value) {
		super(name, value);
	}//Constructor

	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {
	}

	@Override
	public void update(Observable o, Object arg) {
	}

	@Override
	public Number getNumValue() {
		return ( getValue() ? 1 : 0);
	}//getNumValue

}//BooleanAttribute
