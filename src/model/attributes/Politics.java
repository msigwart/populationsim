package model.attributes;


public enum Politics implements EnumAttribute_I{
	TOP_GOVERNMENT(1),
	SENATE(2),
	JUSTICE(3),
	LOWEST(4),
	UNKNOWN(5);
	
	private int number;
	
	Politics(int i) {
		number = i;
	}//Constructor
	
	@Override
	public int getNumValue() {
		return number;
	}//getNumValue
	
	public static Politics getPolitics(int powerIndex) {
		switch (powerIndex) {
			case 1: return TOP_GOVERNMENT;
			case 2: return SENATE;
			case 3: return JUSTICE;
			case 4: return LOWEST;
			case 5:
			default: return UNKNOWN;
		}//switch
	}//getPolitics
	
}//Politics
