package model.attributes;

import java.math.BigDecimal;
import java.util.Observable;

import utility.PageRank;

import Jama.Matrix;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;

public class PageRankAttribute extends AbstractNumericAttribute<BigDecimal> {
	
	/**
	 * Calculates and sets the pageRank for all the families of a specified
	 * FamilyTableModel as well as for the given social net matrix.
	 * @param socialMatrix the social matrix used for the weight calculation
	 * @param families the FamilyTableModel including all the families to make the page rank calculation on
	 * @param hToW the indicator whether the page rank for husbandToWife or wifeToHusband is calculated
	 */
	private static void calculatePageRanks(Matrix socialMatrix, AttributeManagerTableModel<Family> families, boolean hToW) {
		Matrix weights = PageRank.calculateWeights(socialMatrix.getArray());
		Matrix pRankMatrix  = PageRank.getInitPageRank(socialMatrix);
		Matrix newPageRanks = PageRank.calculatePageRankMatrix(pRankMatrix, weights);
//		printPageRanks(newPageRanks);
//				System.out.printf("newPageRanks dimension: %d %d\n", newPageRanks.getRowDimension(), newPageRanks.getColumnDimension());
		setPageRanks(newPageRanks, families, hToW);			//set page ranks for families
		
	}//calculatePageRank
	
	private static void setPageRanks(Matrix pageRanks, AttributeManagerTableModel<Family> families, boolean hToW) {
		for (int i=0; i<pageRanks.getColumnDimension(); i++) {
			double pr = pageRanks.get(0, i);
//					System.out.printf("pageRank: %.20f\n", pr);
			families.getElement(i).setPageRank(new BigDecimal(pr), hToW);
		}//for
	}//setPageRanks
	
	
	
	
	/**
	 * Indicator if the PageRank should be calculated using the husbandToWife matrix
	 * or the wifeToHusband matrix of the socialNet.
	 */
	private boolean hToW;
	
	
	public PageRankAttribute(String name, BigDecimal value, boolean hToW) {
		super(name, value);
		this.hToW		= hToW;

	}//Constructor

	
	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {
		calculatePageRanks(socialNet.getSocialMatrix(hToW), families, hToW);
	}//recalcAttrGlobal

	@Override
	public void update(Observable o, Object arg) {
	}//update


	
}//PageRankAttribute
