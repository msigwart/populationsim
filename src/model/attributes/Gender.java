package model.attributes;


public enum Gender implements EnumAttribute_I {
	UNDEFINED(-1),
	MALE(0),
	FEMALE(1);

	private int numValue;
	
	Gender (int numValue) {
		this.numValue = numValue;
	}//Constructor
	
	@Override
	public int getNumValue() {
		return numValue;
	}//getNumValue
	
	
}//Gender
