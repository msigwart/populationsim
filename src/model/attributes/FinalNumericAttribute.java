package model.attributes;

import java.util.Observable;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;

public class FinalNumericAttribute<T extends Number> extends AbstractNumericAttribute<T> {

	public FinalNumericAttribute(String name, T value) {
		super(name, value);
	}//Constructor
	
	public FinalNumericAttribute(String name) {
		super(name);
	}//Constructor

	@Override
	public void setValue(T v) {
		if (getValue() == null) {
			super.setValue(v);
		}//if
	}//setValue

	
	
	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {		//value should not be modified, because attribute is final
	}//reacalcAttrGlobal


	@Override
	public void update(Observable o, Object arg) {
	}//update
	
}//FinalNumericAttribute
