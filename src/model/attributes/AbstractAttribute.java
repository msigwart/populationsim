package model.attributes;



public abstract class AbstractAttribute<T> implements Attribute_I<T> {
	
	private final String name;
	private 	  T		 value;
	
	
	
	public AbstractAttribute(String name, T value) {
		this.name 	= name;
		this.value 	= value;
	}//Constructor
	

	
	public AbstractAttribute(String name) {
		this.name = name;
	}//Constructor

	public String getAttributeName() {
		return name;
	}//getName
	
	public T getValue() {
		return value;
	}//getValue
	
	public void setValue(T v) {
		value = v;
	}//setValue



}//AbstractFamilyAttribute
