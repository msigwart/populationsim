package model.attributes;

public interface Numerizable {
	Number getNumValue();
}
