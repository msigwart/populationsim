package model.attributes;

import java.util.Observable;


import model.AttributeManagerTableModel;
import model.Family;
import model.Marriage;
import model.SocialNet;

public class ConnectionsAttribute extends AbstractNumericAttribute<Integer> {

	public ConnectionsAttribute(String name, Integer value, Observable o) {
		super(name, value);
		o.addObserver(this);
	}//Constructor

	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {
	}


	private void increaseValue() {
		Integer value = (Integer)getValue();
		value = value + 1;
		setValue(value);
	}//increaseValue
	
	
	private void decreaseValue() {
		Integer value = (Integer)getValue();
		value = value - 1;
		setValue(value);
	}//decreaseValue
	
	
	
	@Override
	public void update(Observable o, Object arg) {
		switch ((int)arg) {
			case Marriage.EVENT_MARRIAGE_STARTED:
				increaseValue();
				break;
			case Marriage.EVENT_MARRIAGE_ENDED:
				decreaseValue();
				break;
			default:
				break;
		}//switch
	}//update

}//ConnectionsAttribute
