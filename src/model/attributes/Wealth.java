package model.attributes;


public enum Wealth implements EnumAttribute_I{
	VERY_RICH(1),
	RICH(2),
	AVERAGE(3),
	POOR(4),
	VERY_POOR(5),
	UNKNOWN(6);
	
	private int numValue;
	
	Wealth(int num) {
		this.numValue = num;
	}//Constructor
	
	public static Wealth getWealth(int wealthIndex) {
		switch (wealthIndex) {
			case 1: return VERY_RICH;
			case 2: return RICH;
			case 3: return AVERAGE;
			case 4: return POOR;
			case 5: return VERY_POOR;
			case 6: 
			default: return UNKNOWN;
		}//switch
	}//getWealth

	@Override
	public int getNumValue() {
		return numValue;
	}//getNumValue
	
}//Wealth
