package model.attributes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import utility.ArrayUtils;

import model.AttributeManagerTableModel;
import model.Family;
import model.SocialNet;

/**
 * This class is a type of manager class to dynamically manage different types of attributes.
 * Attributes need to be of type {@link Attribute_I}.
 * As this class also implements the {@link Attribute_I} interface, {@code AbstractAttributeManager} instances can also be added as attributes.
 * <p>
 * Attributes can be added, deleted and received dynamically. All added attributes are saved in a {@code List}. 
 * The list index of the attribute is also mapped to its attribute name
 * to allow faster and dynamic access of attributes.
 * <p>
 * The class extends {@link Observable}. As the {@code Attribute_I} interface implements the {@link Observer} interface
 * attributes can register and listen for events of {@code AbstractAttributeManager}s
 * 
 * @author Marten Sigwart
 *
 */
public abstract class AbstractAttributeManager extends Observable implements Attribute_I<Integer>{
	
	
	private final String name;
	
	private List<Attribute_I<?>> attributes;
	private List<AbstractAttributeManager> attributeManager;
	
	private Map<String, Integer> attributeMap;
	
	
	/**
	 * Returns a {@code String} array of the default attribute names of the {@code AbstractAttributeManager} instance.
	 * It should be overridden if an {@code AbstractAttributeManager} contains a lot of different attributes, 
	 * but only wants a couple of them to be received by calling classes.
	 * @return A {@code String} array containing all default attribute labels/names.
	 */
	public abstract String[] getDefaultAttrLabels();
//	public abstract boolean[] getSimpleAttrMapping();

	
	public AbstractAttributeManager(String name) {
		this.name		 = name;
		attributes 		 = new ArrayList<Attribute_I<?>>();
		attributeManager = new ArrayList<AbstractAttributeManager>();
		attributeMap	 = new HashMap<String, Integer>();
		
	}//Constructor
	

	
	/**
	 * This method adds an attribute to the {@link AbstractAttributeManager}. If the {@code attrName} already is used for an attribute, 
	 * the existing attribute for this name is deleted and the new attribute is added.
	 * The attribute is added to the list of attributes. Then the name is used as a key to save the list position a map.
	 * If the specified attribute is an instance of {@link AbstractAttributeManager} the attribute is additionally saved in a 
	 * list of AbstractAttributeManagers.
	 * @param attrName the attribute name used as a key to save the list index of the attribute in a map.
	 * @param attribute the attribute to be added to the AbstractAttributeManager.
	 */
	public void addAttribute(String attrName, Attribute_I<?> attribute) {
		//delete attribute if name already exists
		if (attributeMap.containsKey(attrName)) {
			int removeIndex = attributeMap.get(attrName);
			Attribute_I<?> removedAttr = attributes.remove(removeIndex);
			if (removedAttr instanceof AbstractAttributeManager) {
				attributeManager.remove((AbstractAttributeManager)removedAttr);
			}//if
		}//if
		
		//add attribute
		attributes.add(attribute);												//add to attribute list
		attributeMap.put(attrName, attributes.indexOf(attribute));				//attribute name to index
		
		if (attribute instanceof AbstractAttributeManager) {					//if AbstractAttributeManager add to list of AbstractAttributeManager
			attributeManager.add((AbstractAttributeManager)attribute);
		}//if
		
	}//addAttribute
	
	/**
	 * This method adds an attribute to the {@link AbstractAttributeManager}.
	 * It uses the method {@link #addAttribute(String, Attribute_I)}, where the attribute name will be used as the first parameter.
	 * @param attribute the attribute to be added to the AbstractAttributeManager.
	 */
	public void addAttribute(Attribute_I<?> attribute) {
		addAttribute(attribute.getAttributeName(), attribute);
		
	}//addAttribute
	


	/**
	 * Returns the attribute specified by {@code attrName}. 
	 * If the attribute is contained directly in {@link AbstractAttributeManager} the attribute is returned from the list of attributes.
	 * If the attribute is part of an attribute that is an instance of {@code AbstractAttributeManager}, the attribute of that AttributeManager is returned.
	 * If the requested attribute can not be found, {@code null} is returned.
	 * @param attrName the attribute name specifying the requested attribute.
	 * @return the requested attribute or {@code null} if the attribute of name {@code attrName} was not found.
	 */
	public Attribute_I<?> getAttribute(String attrName) {
		if (attrName.contains(".")) {					//composite attribute
			String[] attrParts = attrName.split("\\.");
			
			if (attributeMap.containsKey(attrParts[0])) {
				Integer index = attributeMap.get(attrParts[0]);
				Attribute_I<?> attribute = attributes.get(index);
				if (attribute instanceof AbstractAttributeManager) {
					attrParts = Arrays.copyOfRange(attrParts, 1, attrParts.length);
					return ((AbstractAttributeManager) attribute).getAttribute(ArrayUtils.join(attrParts, '.'));
				}//if
			}//if
			
		} else {
			if (attributeMap.containsKey(attrName)) {
				Integer index = attributeMap.get(attrName);
				return attributes.get(index);
				
			} else if (!attributeMap.containsKey(attrName)) {
				for (AbstractAttributeManager a: attributeManager) {
					if (a.containsAttribute(attrName)) {
						return a.getAttribute(attrName);
					}//if
				}//for
			}//if
		}//if
		return null;
		
	}//getAttribute
	
	
	
	/**
	 * Returns the attribute value specified by {@code attrName}.
	 * The method uses {@link #getAttribute(String)} to get the corresponding attribute.
	 * If the attribute is not {@code null}, the method returns the return value of {@link Attribute_I#getValue()}.
	 * If the attribute was not found by {@link #getAttribute(String)} {@code null} is returned.
	 * @param attrName the attribute name specifying the requested attribute.
	 * @return returns the attribute value if the attribute exists, returns {@code null} if not.
	 */
	public Object getAttributeValue(String attrName) {		
		Attribute_I<?> attribute = getAttribute(attrName);
		if (attribute != null) {
			return attribute.getValue();
		} else {
			return null;
		}//if

	}//getAttributeValue
	
	
	/**
	 * Checks whether a specified attribute exists within the {@link AbstractAttributeManager}. 
	 * If the specified attribute exists either as an attribute or a sub attribute of an {@code AbstractAttributeManager} attribute, the method returns {@code true}.
	 * @param attrName the attribute name specifying the requested attribute.
	 * @return returns {@code true} if the attribute is contained, returns {@code false} if the attribute does not exist.
	 */
	public boolean containsAttribute(String attrName) {
		if (attributeMap.containsKey(attrName)) {
			return true;
		} else {
			for (AbstractAttributeManager a: attributeManager) {
				if (a.containsAttribute(attrName)) {
					return true;
				}//if
			}//for
		}//if
		return false;
	}//containsAttribute
	

	
	/**
	 * Calculates the number of attributes contained in the attribute list
	 * including the attribute count of all contained instances of {@link AbstractAttributeManager}.
	 * @return The total size of attributes and attributes of {@code AbstractAttributeManager}s.
	 */
	public int getAttributeCount() {
		int size = attributes.size();
		for (AbstractAttributeManager a: attributeManager) {
			size += a.getAttributeCount();
		}//for
		return size;
	}//getAttributeCount
	

	
	/**
	 * Returns a {@code String} array of all attribute names contained in the {@code AbstractAttributeManager},
	 * including the names of all attributes of contained {@code AbstractAttributeManager} attributes.
	 * @return A {@code String} array containing all attribute names.
	 */
	public abstract String[] getAllAttributeLabels();
//	public String[] getAllAttributeLabels() {
//		List<String> attrNamesList = new ArrayList<String>();
//		
//		for (String key: attributeMap.keySet()) {
//			attrNamesList.add(key);
//			Attribute_I<?> a = attributes.get(attributeMap.get(key));
//			if (a instanceof AbstractAttributeManager) {
//				List<String> aList = Arrays.asList(((AbstractAttributeManager) a).getDefaultAttrLabels());
//				for (String name : aList) {
//					attrNamesList.add(key + '.' + name);
//				}//for
//			}//if
//		}//for
//		
//		String[] attrNamesArray = new String[5];
//		attrNamesArray = attrNamesList.toArray(attrNamesArray);
//		
//		return attrNamesArray;
//	}//getAttributeNames
	
	
	
	
	
	/**
	 * Creates a string of all attribute values specified by an attribute mapping.
	 * @param attrNames the mapping for the attributes, specifying which attributes are returned in the string
	 * @param numericValues if true, all attributes are displayed by a numeric value.
	 * @return returns a string containing all attribute values specified by mapping. The attribute values are in numeric value if specified by numericValue.
	 */
	public String getAttributeValueString(String[] attrNames, boolean numericValues) {
		if (attrNames.length != getAttributeCount()) {
			throw new IllegalArgumentException();
		}//if
		
		String attrString = "";
		for (String s: attrNames) {
			if (containsAttribute(s)) {
				Attribute_I<?> attribute = getAttribute(s);
				if (numericValues) {
					if (attribute instanceof Numerizable) {
						attrString = attrString + ";" + ((Numerizable) attribute).getNumValue();
					} else {
						attrString = attrString + ";" + attribute.getValue();
					}//if
				} else {
					attrString = attrString + ";" + attribute.getValue();
				}//if
			}//if
		}//for
		return attrString;
	}//getAttributesString
	

	
	
	/**
	 * Returns a list containing all attribute elements of the attributes map.
	 * @return returns a List over AbstractAttributes
	 */
	public List<Attribute_I<?>> getAttributeList() {
		return attributes;	
	}//getAllAttributes
	
	
	/**
	 * Returns a list containing all attribute elements, defined by a boolean array.
	 * @param attrNames the String array to define which attributes are to be added to the list.
	 * @return returns a List over Attribute_I
	 */
	public List<Attribute_I<?>> getAttributeList(String[] attrNames) {
		if (attrNames.length > getAttributeCount()) {
			throw new IllegalArgumentException("String array mapping is the wrong size");
		}//if
		List<Attribute_I<?>> list = new ArrayList<Attribute_I<?>>();
		
		for (int i=0; i<attrNames.length; i++) {
			if (containsAttribute(attrNames[i])) {
				list.add(getAttribute(attrNames[i]));
			}//if
		}//for
		return list;
	}//getAttributeList
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(Integer v) {
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void recalcAttrGlobal(SocialNet socialNet, AttributeManagerTableModel<Family> families) {
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAttributeName() {
		return name;
	}//getName
	
	/**
	 * {@inheritDoc}
	 */
	public String toString() {
		String string = "";
		for (Attribute_I<?> a: attributes) {
			string = string + a.getAttributeName() + ": " + a.getValue() + ", ";
		}//for
		return string;
	}//toString
	

	
	

}//AbstractAttributeManager
