package model;

import java.math.BigDecimal;
import java.util.Observable;

import model.attributes.*;


public class Family extends AbstractAttributeManager {
	
	public static final String ATTRIBUTE_NAME_FAMILY			= "Family";
	public static final String ATTRIBUTE_NAME_ID 				= "FamID";
	public static final String ATTRIBUTE_NAME_FAMILYNAME 		= "Name";
	public static final String ATTRIBUTE_NAME_NO_CONNECTIONS 	= "# Connections";
	public static final String ATTRIBUTE_NAME_PAGERANK_HTOW 	= "PageRank H->W";
	public static final String ATTRIBUTE_NAME_PAGERANK_WTOH 	= "PageRank W->H";
	public static final String ATTRIBUTE_NAME_POLITICS 			= "Politics";
	public static final String ATTRIBUTE_NAME_WEALTH 			= "Wealth";
	public static final String ATTRIBUTE_NAME_POLTICAL_NETWORK 	= "Political Network";
	public static final String ATTRIBUTE_NAME_ORG_ARISTOCRACY 	= "org Aristocracy";
	
	public static final String[] ALL_ATTRIBUTE_NAMES			= { /*ATTRIBUTE_NAME_FAMILY, */ATTRIBUTE_NAME_ID,
																	ATTRIBUTE_NAME_FAMILYNAME, ATTRIBUTE_NAME_NO_CONNECTIONS,
																	ATTRIBUTE_NAME_PAGERANK_HTOW, ATTRIBUTE_NAME_PAGERANK_WTOH,
																	ATTRIBUTE_NAME_POLITICS, ATTRIBUTE_NAME_WEALTH,
																	ATTRIBUTE_NAME_POLTICAL_NETWORK, ATTRIBUTE_NAME_ORG_ARISTOCRACY };


	
	public 	static final int ORIGINAL_ARISTOCRACY	= 0;
	public 	static final int AGGREGATED_ARISTOCRACY = 1;
	
	private static int familyId = 0;
	
	
	
	/**
	 * Resets the static ID counter
	 */
	public static void reset() {
		familyId = 0;
	}//reset
	
	
	
	
//##### Constructors ################
		
	public Family(int id, String name) {
		super(ATTRIBUTE_NAME_FAMILY);
		//Final attributes
		addAttribute(new FinalNumericAttribute<Integer>(ATTRIBUTE_NAME_ID, id));
		addAttribute(new FinalAttribute<String>(ATTRIBUTE_NAME_FAMILYNAME, name));
		
		//Non final attributes
		addAttribute(new ConnectionsAttribute(ATTRIBUTE_NAME_NO_CONNECTIONS, 0, this));
		addAttribute(new PageRankAttribute(ATTRIBUTE_NAME_PAGERANK_HTOW, new BigDecimal(0.0), true));
		addAttribute(new PageRankAttribute(ATTRIBUTE_NAME_PAGERANK_WTOH, new BigDecimal(0.0), false));		
		addAttribute(new EnumAttribute<Politics>(ATTRIBUTE_NAME_POLITICS, Politics.UNKNOWN));
		addAttribute(new EnumAttribute<Wealth>(ATTRIBUTE_NAME_WEALTH, Wealth.UNKNOWN));
		addAttribute(new FinalNumericAttribute<Integer>(ATTRIBUTE_NAME_ORG_ARISTOCRACY));
		addAttribute(new NumericAttribute<Integer>(ATTRIBUTE_NAME_POLTICAL_NETWORK, 0));	
	
	}//Constructor
	
	
	public Family(String name) {
		this(familyId++, name);
		
	}//Constructor

	

	
	// GETTERS //
	
	@Override
	public Integer getValue() {
		return getId();
	}//getValue

	public Integer getId() {
//		return this.id;
		return (Integer)getAttributeValue(ATTRIBUTE_NAME_ID);
	}//getId

	public String getName() {
//		return this.name;
		return (String)getAttributeValue(ATTRIBUTE_NAME_FAMILYNAME);
	}//getName
	
	public Integer getNoOfConnections() {
		return (Integer)getAttributeValue(ATTRIBUTE_NAME_NO_CONNECTIONS);
	}//getNoOfConnections
	
	
	public BigDecimal getPageRank(boolean hToW) {
		if (hToW) {
			return (BigDecimal)getAttributeValue(ATTRIBUTE_NAME_PAGERANK_HTOW);
		} else { 
			return (BigDecimal)getAttributeValue(ATTRIBUTE_NAME_PAGERANK_WTOH);
		}//if
	}//getPageRank
	
	public Politics getPolitics() {
		return (Politics)getAttributeValue(ATTRIBUTE_NAME_POLITICS);
	}//getPolitics

	public Wealth getWealth() {
		return (Wealth)getAttributeValue(ATTRIBUTE_NAME_WEALTH);
	}//getWealth

	public int getOrgAristocracy() {
		return (Integer)getAttributeValue(ATTRIBUTE_NAME_ORG_ARISTOCRACY);
	}//getJoinedAristocracy
	
	
	
	
	// SETTERS //		
	
	public void setPageRank(BigDecimal pR, boolean hToW) {
		if (hToW) {
			((PageRankAttribute)getAttribute(ATTRIBUTE_NAME_PAGERANK_HTOW)).setValue(pR);
		} else {
			((PageRankAttribute)getAttribute(ATTRIBUTE_NAME_PAGERANK_WTOH)).setValue(pR);
		}//if
	}//setPageRank
	
	@SuppressWarnings("unchecked")
	public void setPolitics(Politics p) {
		((EnumAttribute<Politics>)getAttribute(ATTRIBUTE_NAME_POLITICS)).setValue(p);
	}//setPolitics
	
	public void setPolitics(int powerIndex) {
		setPolitics(Politics.getPolitics(powerIndex));
	}//setPolitics
	
	@SuppressWarnings("unchecked")
	public void setWealth(Wealth w) {
		((EnumAttribute<Wealth>)getAttribute(ATTRIBUTE_NAME_WEALTH)).setValue(w);
	}//setWealth
	
	public void setWealth(int wealthIndex) {
		setWealth(Wealth.getWealth(wealthIndex));
	}//setWealth
	
	@SuppressWarnings("unchecked")
	public void setJoinedAristocracy(int j) {
		((FinalNumericAttribute<Integer>)getAttribute(ATTRIBUTE_NAME_ORG_ARISTOCRACY)).setValue(j);
	}//setJoinedAristocracy

	
	
	@Override
	public String[] getDefaultAttrLabels() {
		String[] mapping = {ATTRIBUTE_NAME_ID, ATTRIBUTE_NAME_FAMILYNAME, 
				ATTRIBUTE_NAME_PAGERANK_HTOW, ATTRIBUTE_NAME_PAGERANK_WTOH,
				ATTRIBUTE_NAME_POLITICS, ATTRIBUTE_NAME_WEALTH};
		return mapping;
	}//getDefaultAttrMapping
	
	@Override
	public String[] getAllAttributeLabels() {
		return ALL_ATTRIBUTE_NAMES;
	}


	@Override
	public void update(Observable o, Object arg) {
		switch ((int)arg) {
			case Marriage.EVENT_MARRIAGE_STARTED:
				setChanged();
				notifyObservers(arg);
				break;
			case Marriage.EVENT_MARRIAGE_ENDED:
				setChanged();
				notifyObservers(arg);
				break;
			default:
				break;
		}//switch
	}//update









	
}//Family
