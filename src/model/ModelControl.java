package model;

import java.util.*;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import model.attributes.Attribute_I;
import model.attributes.Gender;
import model.parser.*;



/**
 * This class acts a the control unit of the underlying social data model. 
 * It maintains lists of all {@link Person}, {@link Family}, and {@link Marriage} objects as well as an {@link SocialNet} object, managing the social network.
 * The class takes care of the creation of the data model using parsed data. The parsing of the marriage and family data is taken care by a {@link Parser} object.
 * Furthermore the class is responsible for the initialization, maintaining and reseting the data model.
 * The class also keeps track of the current year of the simulation and works as the main time controlling unit.
 * 
 * @author Marten Sigwart
 *
 */
public class ModelControl {
	
	public static final int	DEFAULT_NO_PRECEDING_YEARS = 20;
	
	public static final Family 		dummyFam 	= new Family(-1, "dummyFam");
	public static final Person 		dummyPer 	= new Person(-1, dummyFam);
	public static final Marriage 	dummyMar 	= new Marriage(-1, dummyPer);
	
	
	
	//Lists
	/**
	 * The list that keeps all parsed all {@link Marriage} objects.
	 */
	private AttributeManagerTableModel<Marriage> 	allMarriages;
	
	//Table Models
	/**
	 * The table model containing all {@link Person} objects of the data model. It represents the population.
	 */
	private AttributeManagerTableModel<Person>		population;
	
	/**
	 * The table model containing all {@link Family} objects.
	 */
	private AttributeManagerTableModel<Family>		families;
	
	/**
	 * The table model containing all past and present {@link Marriage} objects.
	 */
	private AttributeManagerTableModel<Marriage>	marriages;
	
	/**
	 * The table model containing all marriages that are set to start in the current year
	 */
	private AttributeManagerTableModel<Marriage>	almostMarried;
	
	/**
	 * The table model containing one {@link Person} object. It is used for the comparison of one spouse with different potentials.
	 */
	private AttributeManagerTableModel<Person>		spouse;
	
	/**
	 * The table model containing {@link Person} objects representing the potential matches for the person contained in {@link #spouse}.
	 */
	private AttributeManagerTableModel<Person>		potentials;
	
	

	/**
	 * The {@link SocialNet} instance which manages the social matrix of the data model.
	 */
	private SocialNet								socialNet;
	
	
	
	/**
	 * A list of the years where marriages occurred
	 */	
	private List<Integer> 							yearsOfMarriages;
	
	/**
	 * The start year, from this point the statistical analysis of marriages will start.
	 * It will also function as reference point for the preceding years to fill the social net.
	 */
	private int										startYear;
	
	/**
	 * The current year of the simulation. The data model has not been updated with the data of this current year.
	 * That means the marriages of this year have not been processed, and also all marriages ending this year have not
	 * been deleted from the data model.
	 */
	private int										currentYear;
	
	/**
	 * The number of preceding years, to build the data model before the statistical analysis
	 */
	@SuppressWarnings("unused")
	private int										noOfPrecedingYears;
	

	private Parser									parser;
	private DefaultComboBoxModel<Integer>			currentYearModel;
	private DefaultComboBoxModel<String>  			potentialComboBoxModel;


	


	
	
	public ModelControl() {
		//Models for views
		this.allMarriages			= new AttributeManagerTableModel<Marriage>(dummyMar.getAllAttributeLabels());
		this.population				= new AttributeManagerTableModel<Person>(dummyPer.getDefaultAttrLabels());
		this.families 				= new AttributeManagerTableModel<Family>(dummyFam.getAllAttributeLabels());
		this.marriages 				= new AttributeManagerTableModel<Marriage>(dummyMar.getDefaultAttrLabels());
		this.almostMarried 			= new AttributeManagerTableModel<Marriage>(dummyMar.getSimpleAttrLabels()); 
		this.spouse					= new AttributeManagerTableModel<Person>(dummyPer.getAllAttributeLabels());
		this.potentials				= new AttributeManagerTableModel<Person>(dummyPer.getAllAttributeLabels());
		this.currentYearModel 		= new DefaultComboBoxModel<Integer>();
		this.potentialComboBoxModel = new DefaultComboBoxModel<String>() {			//anonymous class
			
			
			@Override
			public void setSelectedItem(Object item) {
				if (item.toString().startsWith("**")) {
					return;
				}//if
				super.setSelectedItem(item);
			}//setSelectedItem
		};
		
		init();
	}//Constructor
	
	/**
	 * This method initializes the data model. It sets the parser, initiates the {@code allMarriages} list and the {@link SocialNet} instance.
	 * Furthermore all table models are emptied. Year variables like {@code startYear} and {@code currentYear} are set to default values.
	 * At last the static id counters of classes {@link Family}, {@link Marriage} and {@link Person} are reset.
	 * This method can also be used to reset the data model.
	 */
	public void init() {
		//Parser
		this.parser			= new Parser("marriages.csv", "familyInfo.csv");

		//Lists
//		this.allMarriages	= new ArrayList<Marriage>();
		this.socialNet		= new SocialNet();
		
		//Remove table model content
		this.allMarriages.removeAll();
		this.population.removeAll();
		this.families.removeAll();
		this.marriages.removeAll();
		this.almostMarried.removeAll();
		this.spouse.removeAll();
		this.potentials.removeAll();
		
		//Remove combobox model content
		this.currentYearModel.removeAllElements();
		
		
		this.startYear			= -1;		//default initialization
		this.currentYear		= -1;		//default initialization
		this.noOfPrecedingYears = DEFAULT_NO_PRECEDING_YEARS;

		
		Family.reset();
		Marriage.reset();
		Person.reset();
	}//init
	
	
//############### CREATION METHODS ############################################################################
	
	/**
	 * Returns the {@link Family} object of a certain name from the {@link #families} table.
	 * If the family does not exist yet a new family is created and returned with {@link #createNewFamily(String)}.
	 * @param name the family name to search for
	 * @return the found/created {@code Family} object.
	 */
	private Family getFamilyWithName(String name) {
		for (Family f: families.getElements()) {
			if (f.getName().equals(name)) {				//! equals()
				return f;
			}//if
		}//for
		
		return createNewFamily(name);
		
	}//getFamilyWithName
	
	
	/**
	 * Creates new {@link Family} object and adds it to the list of families.
	 * @param name the name of the family
	 * @return The newly created {@link Family} object.
	 */
	private Family createNewFamily(String name) {
		Family newFam = new Family(name);
		families.addRow(newFam);
		return newFam;
		
	}//createNewFamily
	
	/**
	 * Creates a new {@link Person} object and adds it to the population list.
	 * @param gender the {@link Gender} of the person
	 * @param family the {@link Family} of the person
	 * @return The newly created {@link Person} object.
	 */
	private Person createNewPerson(Gender gender, Family family) {
		Person newPer = new Person(gender, family);
		population.addRow(newPer.getId(), newPer);
		return newPer;
		
	}//createNewPerson
	
	/**
	 * Creates a new {@link Marriage} object using parsed marriage info contained in a {@link MarriageObject}. 
	 * First the {@link Person} objects for husband and wife are created. Then the new marriage is created using husband and wife, 
	 * as well as the start and end year of the marriage. 
	 * @param p The {@link MarriageObject} object containing the parsed marriage data.
	 * @return returns the newly created {@link Marriage} object
	 */
	private Marriage createNewMarriage(MarriageObject p) {
		
		String 	husbandName = p.getHusbandName();						//husband name
		String 	wifeName	= p.getWifeName();							//wife name
		int		startYear	= p.getStartMarriage();						//marriage start
		int		endYear		= p.getEndMarriage();						//marriage end
		
		//creation of husband
		Family husbandFam;
		husbandFam			= getFamilyWithName(husbandName);
		Person husband 		= createNewPerson(Gender.MALE, husbandFam);
		socialNet.newFamily(husbandFam.getId());

		
		//creation of wife
		Family wifeFam;
		wifeFam				= getFamilyWithName(wifeName);
		Person wife			= createNewPerson(Gender.FEMALE, wifeFam);
		socialNet.newFamily(wifeFam.getId());

		
		//creation of marriage
		return new Marriage(husband, wife, startYear, endYear);
		
	}//createNewMarriage
	
	
	
	
//############### PARSE METHODS ############################################################################

	/**
	 * Causes the {@link Parser} object to parse the marriage file for basic info. 
	 * This way the data model finds out for which years marriage data exists.
	 * @param marriageFile 
	 * @throws NullPointerException is thrown when {@code Parser} returns null.
	 */
	public void parseBasicInfo(String marriageFile) {
		yearsOfMarriages = parser.parseBasicInfo(marriageFile);
		if (yearsOfMarriages == null) {
			throw new NullPointerException("Something went wrong reading the basic info from file");
		}//if
	}//parseBasicInfo
	
	
	/**
	 * Causes the {@link Parser} object to parse the marriage file for all marriages. 
	 * All parsed marriages are added to the {@link #allMarriages} list.
	 */
	public void parseMarriages() {
		for (MarriageObject p: parser.parseMarriages()) {
			allMarriages.addRow(createNewMarriage(p));
		}//for
		System.out.printf("Parsed %d marriages\n", allMarriages.getRowCount());
	}//parseMarriages
	
	
	/**
	 * Causes the {@link Parser} object to parse the family file for all family info.
	 */
	public void parseFamilyInfo() {
		for (FamilyObject f: parser.parseFamilyInfo()) {
			Family family = getFamilyWithName(f.getFamilyName());
			
			//Set family values
			if (family != null) {
				//System.out.printf("Setting attributes for family %s\n", family.getName());
				family.setPolitics(f.getPolitics());					//set political power
				family.setWealth(f.getWealth());						//set wealth
				family.setJoinedAristocracy(f.getOrgAristocracy());	//set joined aristocracy
				
			} else {
				System.out.printf("Family %s does not exist in data model\n", f.getFamilyName());
			}//if
		}//for
	}//parseFamilyInfo
	
	/**
	 * Sets {@link #startYear} and {@link #noOfPrecedingYears} of the data model. 
	 * The data model will be built up until {@link #startYear} using {@link #noOfPrecedingYears} prior.
	 * @param startYear the start year
	 * @param noOfPrecedingYears the number of preceding years
	 */
	public void setParseOptions(int startYear, int noOfPrecedingYears) {
		this.startYear 			= startYear;
		this.noOfPrecedingYears = noOfPrecedingYears;
		this.currentYear		= startYear-noOfPrecedingYears;
		System.out.printf("Processing marriages from %d to %d\n", currentYear, startYear);
		
	}//setParseOptions
	
	
	public void setMarriageFile(String marriageFile) {
		parser.setMarriageFile(marriageFile);
	}//setMarriageFile
	
	public void setFamilyFile(String familyFile) {
		parser.setFamilyFile(familyFile);
	}//setFamilyFile

	public boolean isValidMarriageFile(String marriageFile) {
		return parser.isValidMarriageFile(marriageFile);
	}//isValidMarriageFile
	
	public boolean isValidFamilyFile(String familyFile) {
		return parser.isValidFamilyFile(familyFile);
	}//isValidFamilyFile
	
	
	
//############### PROCESSING METHODS ############################################################################
	
	/**
	 * Sets the data model to the specified year. 
	 * <p>
	 * If the specified year is bigger than {@link #currentYear} all years up to the specified year 
	 * are processed and the marriages of the new current year are extracted.
	 * <p>
	 * If the specified year is smaller than {@link #currentYear} the model is reset to the specified year.
	 * <p>
	 * <B>In all cases the specified year will become the new {@link #currentYear}.</B>
	 * 
	 * @param year the specified year
	 */
	public void setModelToYear(int year) {
		if (year > getCurrentYear()) {			//process years as usual
			moveToYear(year);
			
		} else if (year < getCurrentYear()) {	//data model needs to be reset
			resetModelToYear(year);
		} else {
			//do nothing everything stays the same
		}//if
		updateNetworkAttributes();
		updateTables();
		updatePotentialsCmbModel();
	}//setModelToYear
	
	
	
	/**
	 * Processes marriages of all years specified by parameters.
	 * Going from year to year, it extracts the marriages of each year from the allMarriages list to 
	 * the toBeMarried list, then processes the current year (activating and ending marriages).
	 * @param from the start year
	 * @param to the end year
	 */
	private void moveToYear(int to) {
		for (int year = getCurrentYear(); year < to; year++) {
			moveToNextYear();
		}//for		
	}//processMarriages
	
	
	/**
	 * Processes all 'events' of the current year. All marriages from the {@link #almostMarried} table 
	 * are processed and the links are created in the {@link SocialNet} object. All marriages set to end in this year are ended.
	 * Finally the current year field is increased and the {@code Marriage} objects for the new year are extracted.
	 */
	private void moveToNextYear() {
		startCurrentMarriages();
		endMarriages(currentYear);
		System.out.printf("Processed year %d\n", currentYear);
		setCurrentYear(currentYear+1);
		extractMarriagesForCurrentYear();
		
	}//processCurrentYear
	
	/**
	 * Sets the current year of the simulation. The current year is only set if the parameter doesn't match the field.
	 * @param year the new current year
	 */
	private void setCurrentYear(int year) {
		if (this.currentYear != year) {
			this.currentYear = year;
			System.out.printf("Current year: %d\n", this.currentYear);
		}//if
		
	}//setCurrentYear
	
	
	/**
	 * Starts all {@code Marriage} objects from the {@link #almostMarried} table. At the end all elements are removed from the table.
	 */
	private void startCurrentMarriages() {
		for (Marriage m: almostMarried.getElements()) {
			startMarriage(m);
		}//for
		almostMarried.removeAll();
		
	}//processMarriages
	
	/**
	 * Start a {@code Marriage} object. It is added to the {@link #marriages} table and the link in the social network is created.
	 * @param m the {@code Marriage} to be started
	 */
	private void startMarriage(Marriage m) {
		if (!marriages.hasElement(m)) {			//otherwise it might be added multiple times
			marriages.addRow(m);
		}//if
		socialNet.startMarriage(m);
		m.startMarriage();					//IMPORTANT: has to be called AFTER social net call

	}//activateMarriage
	
	
	/**
	 * Ends all marriages from the specified year. 
	 * @param year the end year
	 */
	private void endMarriages(int year) {
		for (Marriage m: marriages.getElements()) {
			if (m.getEndOfMarriage() == year) {
				m.endMarriage();						//set marriage to inactive, IMPORTANT: has to be called BEFORE social net call
				socialNet.endMarriage(m);				//delete link in social net matrix
			}//if
		}//for
	}//endMarriages
	
	
	/**
	 * Extracts marriages for the current year from the {@link #allMarriages} list. Current year is specified by the {@link #currentYear} field.
	 * The extracted marriages are added to the {@link #almostMarried} table. The links in the social net matrix are <B>not</B> yet created.
	 */
	private void extractMarriagesForCurrentYear() {
		for (Marriage m: allMarriages.getElements()) {
			if (m.getStartOfMarriage() == currentYear) {
				String status = String.format("Husband:%-20sWife:%-20sYear: %d  EndYear: %d\n", 
						m.getHusbandName(), m.getWifeName(), m.getStartOfMarriage(), m.getEndOfMarriage());
				System.out.print(status);
				almostMarried.addRow(m);
			}//if
		}//for
	}//extractMarriagesForCurrentYear
	
	

	
//############### RESETING METHODS ############################################################################

	/**
	 * Resets the model to a specified year
	 * @param year the new current year
	 */
	private void resetModelToYear(int year) {
		almostMarried.removeAll();
		resetMarriagesUntil(year);
		setCurrentYear(year);
		
	}//resetModelToYear
	
	
	
	/**
	 * Resets the data model up to a specific year. All marriages of the {@link #marriages} table are ended.
	 * The link in the {@link SocialNet} object is removed and the {@code Marriage} is set to inactive.
	 * {@link Person} objects husband and wife are removed from the population table.//TODO are they actually removed?
	 * Finally the {@code Marriage} objects are removed from the {@link #marriages} table.
	 * @param year the specified year
	 */
	private void resetMarriagesUntil(int year) {
		Iterator<Marriage> iterator = marriages.getElements().iterator();		//use iterator to remove objects while iterating
		while (iterator.hasNext()) {
			Marriage m = iterator.next();
			
			//delete all marriages after year
			if (m.getStartOfMarriage() > year) {
//				if (m.isMarriageActive()) {
					m.endMarriage();								//set to inactive, decrease no. of connections of families
					socialNet.endMarriage(m);						//decrease link in social net matrix
//				}//if
				iterator.remove();								//remove object from marriages list
			
			//handle marriages of year
			} else if (m.getStartOfMarriage() == year) {
				m.endMarriage();
				socialNet.endMarriage(m);
				almostMarried.addRow(m);
				iterator.remove();

			}//if
			
			//reactivate marriages from year up until recent year
			if (m.getEndOfMarriage() < currentYear && m.getEndOfMarriage() >= year) {
				socialNet.startMarriage(m);
				m.startMarriage();

			}//if
			
		}//while

		
	}//deleteMarriagesUntil
	
	
//############### UPDATE METHODS ############################################################################

	private void updateTables() {
		if (families.size() > 0) {
			families.fireTableDataChanged();
		}//if
		if (marriages.size() > 0) {
			marriages.fireTableDataChanged();
		}//if
		if (almostMarried.size() > 0) {
			almostMarried.fireTableDataChanged();
		}//if
	}//updateTables
	
	/**
	 * Updates the {@link #spouse} and {@link #potentials} tables. When a new {@code Person} spouse is selected for comparison
	 * this method updates both tables, making them available to possibly connected views.
	 */
	public void updatePotentialTables() {
		//remove all from potentials and spouse list
		potentials.removeAll();
		spouse.removeAll();
		
		//get selected person
		if (potentialComboBoxModel.getSelectedItem() != null) {
			int selectedId 	= Integer.parseInt( ((String)potentialComboBoxModel.getSelectedItem()).split(" ")[0]);
			Person selected = getPersonWithId(selectedId);
				
			//add data
			for (Marriage m: almostMarried.getElements()) {
				Person potential = null;
				boolean orgPartner = false;			//indicator if the selected person is actually part of the current marriage
				if (selected == m.getHusband() || selected == m.getWife()) {	//we now reached the marriage which contains the selected Person
					orgPartner = true;
				}//if
				switch (selected.getGender()) {
					case MALE:
						potential = m.getWife();
						break;
					case FEMALE:
						potential = m.getHusband();
						break;
					default:
						break;
				}//switch
				if (potential != null) {
					potential.setOrgPartner(orgPartner);
					potentials.addRow(potential);
				}//if

			}//for
			selected.setOrgPartner(true);
			spouse.addRow(selected);
			spouse.fireTableDataChanged();
			
			potentials.fireTableDataChanged();
		}//if
		
	}//updatePotentialsTableModel
	
	/**
	 * Updates the ComboBoxModel for potential spouses. It adds all of the spouses of the currently selected year to the combo box.
	 */
	private void updatePotentialsCmbModel() {
		potentialComboBoxModel.removeAllElements();
		List<String> potWives	= new ArrayList<String>();		//potential wives
		
		potentialComboBoxModel.addElement("*** HUSBANDS ***");
		
		for (Marriage m: almostMarried.getElements()) {
			potentialComboBoxModel.addElement(m.getHusband().getId() + " " + m.getHusbandName());
			potWives.add(m.getWife().getId() + " " + m.getWifeName());
		}//for
		potentialComboBoxModel.addElement("*** WIVES ***");
		for (String s: potWives) {
			potentialComboBoxModel.addElement(s);
		}//for
	}//updatePotentialCmbModel
	
	
	public void updateCurrentYearCmbModel() {
		currentYearModel.removeAllElements();
		Integer[] statYears = Arrays.copyOf(getYearsOfMarriages().toArray(), getYearsOfMarriages().toArray().length, Integer[].class);
		for (int i=0; i<statYears.length; i++) {
			if (statYears[i] >= startYear) {
				if (statYears[i] == startYear) {
					currentYearModel.setSelectedItem(statYears[i]);
				}//if
				currentYearModel.addElement(statYears[i]);
			}//if

		}//for
	}//updateCurrentYearCmbModel
	
	


	
//############### ATTRIBUTE GLOBAL RECALCULATION ############################################################################

	/**
	 * Updates all Network attributes. The method iterates through all existing {@link Family} attributes and 
	 * calls the {@link Attribute_I#recalcAttrGlobal(SocialNet, AttributeManagerTableModel)} method.
	 */
	private void updateNetworkAttributes() {						//TODO  should maybe iterate through all, not only attributes of Family
		for (Attribute_I<?> a: dummyFam.getAttributeList()) {
			a.recalcAttrGlobal(socialNet, families);
		}//for
		System.out.printf("There exist %d families in the data model\n", families.getRowCount());
	}//updateNetworkAttributes
	
	
//############### GETTERS & SETTERS ############################################################################	
	/**
	 * @return returns the SocialNet object
	 */
	public SocialNet getSocialNet() {
		return socialNet;
	}//getSocialNet

	/**
	 * @Return returns the FamilyTableModel for all families
	 */
	public AttributeManagerTableModel<Family> getFamilies() {
		return families;
	}//getFamilyTableModel

	/**
	 * @return returns the MarriageTableModel from field marriages.
	 */
	public AttributeManagerTableModel<Marriage> getMarriages() {
		return marriages;
	}//getMarriagesTableModel

	/**
	 * @return returns the AlmostMarriedTableModel from field toBeMarried.
	 */
	public AttributeManagerTableModel<Marriage> getAlmostMarried() {
		return almostMarried;
	}//getAlmostMarriedTableModel

	/**
	 * @return returns the PotentialsTableModel from field potentials.
	 */
	public AttributeManagerTableModel<Person> getPotentials() {
		return potentials;
	}//getPotentialsTableModel

	/**
	 * @return returns the PotentialsTableModel from field spouse.
	 */
	public AttributeManagerTableModel<Person> getSpouse() {
		return spouse;
	}//getSpouseTableModel

	
//	public List<Integer> getYearsOfMarriages() {
//		return yearsOfMarriages;
//	}//getYearsOfMarriage
	
	public List<Integer> getYearsOfMarriages() {
		return yearsOfMarriages;
	}//getYearsOfMarriagesArray
	
	public int getStartYear() {
		return startYear;
	}//getStartYear;
	
	public void setStartYear(int year) {
		this.startYear = year;
	}//setStartYear
	
	public void setNoOfPrecedingYears(int years) {
		this.noOfPrecedingYears = years;
	}//setNoOfPrecedingYears
	
	public int getCurrentYear() {
		return currentYear;
	}//getCurrentYear
	
	public int getLastYear() {
		return yearsOfMarriages.get(yearsOfMarriages.size()-1);
	}//getLastYear


	private Person getPersonWithId(int id) {
		return population.getElement(id);
	}//getPersonWithId

//TODO not necessary
//	public Integer[] getStatYears() {
//		List<Integer> years = new ArrayList<Integer>();
//		Integer[] statYears = new Integer[5];
//		for (Integer i: yearsOfMarriages) {
//			if (i >= startYear+1) {
//				years.add(i);
//			}//if
//		}//for
//		return years.toArray(statYears);
//		
//	}//getStatYears
	
	
	/**
	 * @return returns the ComboBoxModel from field currentYearModel.
	 */
	public ComboBoxModel<Integer> getCurrentYearComboBoxModel() {
		return currentYearModel;
	}//getCurrentYearComboBoxModel
	
	public DefaultComboBoxModel<String> getPotentialsCmbModel() {		
		return potentialComboBoxModel;
	}//getPotentialsCmbModel
	

	
	
	
//############### PRINT METHODS ############################################################################

	/**
	 * Prints the social matrix to the console
	 * @param hToW boolean whether to print the husbandToWife or the wifeToHusband matrix
	 */
	public void printSocialNet(boolean hToW) {
		socialNet.printNet(hToW);
		
	}//displaySocialNet
	
	
	/**
	 * Prints the names of all families
	 */
	public void printFamilies() {
		for (Family f: families.getElements()) {
			System.out.println(f);
		}//for
		
	}//displayFamilies




		

	
	
	
}//Control
