package model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import Jama.Matrix;

public class SocialNet {
	
	private static final int INITIAL_SIZE = 10;
	
	private double[][] husbandToWife;
	private double[][] wifeToHusband;
	
	
	
	
	public SocialNet() {
		this.husbandToWife = new double[INITIAL_SIZE][INITIAL_SIZE];
		this.wifeToHusband = new double[INITIAL_SIZE][INITIAL_SIZE];

		initNet();
	}//Constructor
	
		
	/**
	 * Returns the social net as a Matrix object.
	 * @param hToW if true returns the husbandToWife matrix, if false returns wifeToHusband
	 * @return the social matrix corresponding to paramater hToW
	 */
	public Matrix getSocialMatrix(boolean hToW) {
		if (hToW) {
			return new Matrix(husbandToWife);
		} else {
			return new Matrix(wifeToHusband);
		}//if
	}//getSocialMatrix
	
	
	/**
	 * Adds a new family to the matrix
	 * @param famId
	 */
	public void newFamily(int famId) {
		if (famId >= husbandToWife.length-1) {
			resizeNets(famId+1);
		}//for
		for (int i=0; i<husbandToWife.length; i++) {
			husbandToWife[i][famId] = 0;
			husbandToWife[famId][i]	= 0;
			wifeToHusband[i][famId] = 0;
			wifeToHusband[famId][i]	= 0;
		}//for
		husbandToWife[famId][famId] = 0;
		wifeToHusband[famId][famId] = 0;
		
	}//newFamily
	
	
	/**
	 * Creates a new marriage link in the social net matrix
	 * @param marriage
	 */
	public void startMarriage(Marriage marriage) {
		Family husbandFam 	= marriage.getHusband().getFamily();
		Family wifeFam 		= marriage.getWife().getFamily();

		if ( husbandFam.getNoOfConnections() == 0 ) {
			husbandToWife[husbandFam.getId()][husbandFam.getId()] = 1;
			wifeToHusband[husbandFam.getId()][husbandFam.getId()] = 1;
		}//if
		
		if ( wifeFam.getNoOfConnections() == 0 ) {
			husbandToWife[wifeFam.getId()][wifeFam.getId()] = 1;
			wifeToHusband[wifeFam.getId()][wifeFam.getId()] = 1;
		}//if
		
		husbandToWife[husbandFam.getId()][wifeFam.getId()] += 1;
		wifeToHusband[wifeFam.getId()][husbandFam.getId()] += 1;

	}//newMarriage
	
	/**
	 * Decreases the marriage link counter in the social net matrix
	 * @param marriage
	 */
	public void endMarriage(Marriage marriage) {
		Family husbandFam 	= marriage.getHusband().getFamily();
		Family wifeFam 		= marriage.getWife().getFamily();
		
		husbandToWife[husbandFam.getId()][wifeFam.getId()] -= 1;
		wifeToHusband[wifeFam.getId()][husbandFam.getId()] -= 1;
		
		if ( husbandFam.getNoOfConnections() == 0 ) {
			husbandToWife[husbandFam.getId()][husbandFam.getId()] = 0;
			wifeToHusband[husbandFam.getId()][husbandFam.getId()] = 0;
		}//if
		
		if ( wifeFam.getNoOfConnections() == 0 ) {
			husbandToWife[wifeFam.getId()][wifeFam.getId()] = 0;
			wifeToHusband[wifeFam.getId()][wifeFam.getId()] = 0;
		}//if
		
	}//endMarriage


	/**
	 * Initializes the social net matrix with -1 for each value
	 */
	private void initNet() {
		for (int i=0; i<INITIAL_SIZE; i++) {
			for (int j=0; j<INITIAL_SIZE; j++) {
				husbandToWife[i][j] = -1;
				wifeToHusband[i][j] = -1;
			}//for
		}//for
	}//initNet
	
	
	/**
	 * This method resizes the social net, e.g. when a new family occurs
	 * @param newSize the new size of the net
	 */
	private void resizeNets(int newSize) {
		double newHToW[][] = new double[newSize][newSize];		//create new array with new size
		double newWToH[][] = new double[newSize][newSize];		//create new array with new size

		for (int i=0; i<husbandToWife.length; i++) {
			for (int j=0; j<husbandToWife.length; j++) {
				newHToW[i][j] = husbandToWife[i][j];			//copy values
				newWToH[i][j] = wifeToHusband[i][j];
			}//for
		}//for
		husbandToWife = newHToW;								//make the swap
		wifeToHusband = newWToH;
	}//resizeNet
	
	

	
	
	public void printNet(boolean hToW) {
		if (hToW) {
			printNet(husbandToWife);
		} else {
			printNet(wifeToHusband);
		}//if
	}//printNet
	
	private void printNet(double[][] net) {
		for (int i=0; i<net.length; i++) {
			for (int j=0; j<net.length; j++) {
				System.out.printf("%f\t", net[i][j]);
			}//for
			System.out.printf("\n");
		}//for
	}//printNet
	
	
	public void writeMatrixToFile(String filename, boolean hToW) {
		if (hToW) {
			writeMatrixToFile(filename, husbandToWife);
		} else {
			writeMatrixToFile(filename, wifeToHusband);
		}//if
	}//writeMatrixToFile

	
	private void writeMatrixToFile(String filename, double[][] net) {
		PrintWriter pw;
		try {
			pw = new PrintWriter(filename);
			
			pw.printf("A=[\n");
			for (int i=0; i<net.length; i++) {
				for (int j=0; j<net.length; j++) {
					pw.printf("%.0f ", net[i][j]);
				}//for
				if (i==net.length-1) {
					pw.printf("]\n");
				} else {
					pw.printf(";\n");
				}//if
			}//for
			pw.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}//catch
	}//writeMatrixToFile
	
}//SocialNet
