package model;

import java.util.*;



public class Statistics {
	
	//ANSI escape codes
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	
	
	
	private List<Person> 	husbands;
	private List<Person> 	wives;

	
	
	public Statistics() {
		husbands 		= new ArrayList<Person>();
		wives			= new ArrayList<Person>();
		
		
	}//Constructor

	

	public void displayPersonsToBeMarried() {
		husbands.clear();
		wives.clear();
		//husbands 	= parser.getPersonsToBeMarried(true);
		//wives		= parser.getPersonsToBeMarried(false);
		
		System.out.printf("\nHusbands:\n");
		for (Person p: husbands) {
			System.out.printf("%s\n", p);
		}//for
		
		System.out.printf("\nWives:\n");
		for (Person p: wives) {
			System.out.printf("%s\n", p);
		}//for
		
	}//displayPersonsToBeMarried
	
	
	
	public void matchComparison(int index, boolean male) {
		Person p;
		if (male) {
			p = husbands.get(index);
		} else {
			p = wives.get(index);
		}//if
		System.out.printf("\nMatch comparison for %s:\n", p);
		System.out.printf("Name                | Politics | Wealth   | Connections\n" +
						  "--------------------+----------+----------+------------\n");
		if (male) {
			for (int i=0; i<wives.size(); i++) {
				Person w = wives.get(i);
				
				//set color for terminal
				if (i==index) {
					System.out.printf("%s", ANSI_RED);
				} else {
					System.out.printf("%s", ANSI_RESET);
				}//if
				System.out.printf("%-20s|%-10s|%-10s|%d\n",
						w.getFamily().getName(), w.getFamily().getPolitics(), 
						w.getFamily().getWealth(), w.getFamily().getNoOfConnections());
		
			}//for
			
		} else {
			for (int i=0; i<husbands.size(); i++) {
				Person w = husbands.get(i);
				
				//set color for terminal
				if (i==index) {
					System.out.printf("%s", ANSI_RED);
				} else {
					System.out.printf("%s", ANSI_RESET);
				}//if
				System.out.printf("%-20s|%-10s|%-10s|%d\n",
						w.getFamily().getName(), w.getFamily().getPolitics(),
						w.getFamily().getWealth(), w.getFamily().getNoOfConnections());
			
			}//for
			
		}//else
		
	}//matchComparison
	
	
}//Statistics
