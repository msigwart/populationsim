package model.parser;


public class FamilyObject extends ParseObject {
	
	private final String 	familyName;
	private final int	 	politics;
	private final int 		wealth;
	private final int 		orgAristocracy;
	
	
	public FamilyObject(String familyName, int politics, int wealth, int orgAristocracy) {
		super(PARSE_OBJECT_FAMILY);
		this.familyName = familyName;
		this.politics = politics;
		this.wealth = wealth;
		this.orgAristocracy = orgAristocracy;
		
	}//Constructor


	public String getFamilyName() {
		return familyName;
	}//getFamilyName


	public int getPolitics() {
		return politics;
	}//getPolitics

	public int getWealth() {
		return wealth;
	}//getWealth

	public int getOrgAristocracy() {
		return orgAristocracy;
	}//getJoinedAristocracy
	
}//FamilyObject
