package model.parser;

public class ParseObject {
	
	public static int PARSE_OBJECT_MARRIAGE = 0;
	public static int PARSE_OBJECT_FAMILY	= 1;
	
	
	private final int parseObjectType;
	
	public ParseObject(int parseObjectType) {
		this.parseObjectType = parseObjectType;
	}//Constructor
	
	public int getParseObjectType() {
		return parseObjectType;
	}//getParseObjectType
	
}//ParseObject
