package model.parser;

public class MarriageObject extends ParseObject {
	
	private String 	husbandName;
	private String 	wifeName;
	private int		startMarriage;
	private int		endMarriage;
	
	
	
	public MarriageObject(String husbandName, String wifeName, int startMarriage, int endMarriage) {
		super(PARSE_OBJECT_MARRIAGE);
		this.husbandName = husbandName;
		this.wifeName = wifeName;
		this.startMarriage = startMarriage;
		this.endMarriage = endMarriage;
	}//Constructor
	

	public String getHusbandName() {
		return husbandName;
	}//getHusbandName
	
	public String getWifeName() {
		return wifeName;
	}//getWifeName
	
	public int getStartMarriage() {
		return startMarriage;
	}//getStartMarriage
	
	public int getEndMarriage() {
		return endMarriage;
	}//getEndMarriage
	
	
	
}//ParseObject
