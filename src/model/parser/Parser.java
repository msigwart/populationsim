package model.parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;


/**
 * Class Parser to extract marriage information from a specified file.
 * @author Marten Sigwart
 *
 */
public class Parser {

	
	//Indexes for parsing of marriage file
	public static final int INDEX_NAME_HUSBAND 		= 0;
	public static final int INDEX_NAME_WIFE			= 1;
	public static final int INDEX_MARRIAGE_START 	= 2;
	public static final int INDEX_MARRIAGE_END		= 3;
	
	//Indexes for parsing of family information
	public static final	int	INDEX_FAMILY_NAME		= 0;
	public static final int INDEX_POLITICAL_POWER	= 1;
	public static final int	INDEX_WEALTH			= 2;
	public static final int INDEX_JOINED_ARSTCRCY	= 3;
		
	
	
	
	private String			marriageFile;
	private String			familiesFile;
	
	/** 
	 * The reader responsible to parse through marriage file
	 */
	private BufferedReader  marriageReader;
	
	
	
	
	
//##### Constructors ################
	
	public Parser(String marriageFile, String familiesFile) {
		this.marriageFile 	= marriageFile;
		this.familiesFile	= familiesFile;
		
	}//Constructor

	
	
	
//##### Parse Methods ################

	/**
	 * This method parses the marriage file for basic information such as the years of marriages, no. of families, etc.
	 * @param marriageFile 
	 * @return returns an Integer array containing all marriage years found in the file
	 */
	public List<Integer> parseBasicInfo(String marriageFile) {
		System.out.printf("Collecting basic information...\n");
		try {
			marriageReader = new BufferedReader(new FileReader(marriageFile));
			List<Integer> yearsOfMarriages = new ArrayList<Integer>();
			
			String line = marriageReader.readLine();		//read first line
			int noOfLines = 0;
			while (line != null) {
				if (line.contains(";")) {		// is document .csv format?
					String parts[] = line.split(";");
					Integer year = Integer.parseInt(parts[INDEX_MARRIAGE_START]);
					if (!yearsOfMarriages.contains(year)) {
						yearsOfMarriages.add(year);
					}//if
				}//if
				
				line = marriageReader.readLine();			//read next line
				noOfLines++;
			}//while
			marriageReader.close();							//close reader
			marriageReader = null;							//set reader to null
			
			System.out.printf("Done - Read %d lines.\n", noOfLines);
			return yearsOfMarriages;
			
		} catch (IOException e) {
			e.getStackTrace();
			return null;
		}//catch
		
	}//parseBasicInfo
	
	
	/**
	 * This method parses the marriage file for the recorded marriages and saves them in the data model.
	 * @return returns a list of parse objects containing the marriages
	 */
	public List<MarriageObject> parseMarriages() {
		List<MarriageObject> pMarriages = new ArrayList<MarriageObject>();		//list for parsed marriages
		
		try {
			marriageReader = new BufferedReader(new FileReader(marriageFile));
			
			//start parsing
			String line = marriageReader.readLine();
			while (line != null) {
				
				String[] parts = line.split(";");			//the string array containing marriage info
				pMarriages.add(parseMarriage(parts));		//parse marriage, returns ParseObject and adds it to list
				line = marriageReader.readLine();
									
			}//while
			
			marriageReader.close();			//newly read lines are null --> close stream
			marriageReader = null;
			return pMarriages;
						
		} catch (IOException e) {
			e.getStackTrace();
			System.out.printf("oh no something went wrong: %s\n", e);
			return null;
			
		}//catch
		
	}//parseMarriages
	
	
	/**
	 * This method parses the marriage file for the recorded marriages and saves them in the data model.
	 * @param from the year to start the parsing from
	 * @param to the year to end the parsing in
	 */
	public List<MarriageObject> parseMarriages(int from, int to) {
		List<MarriageObject> pMarriages = new ArrayList<MarriageObject>();		//list for parsed marriages
		
		try {
			if (marriageReader == null) {				//start of marriage parsing
//				System.out.printf("Created new Buffered reader\n");
				marriageReader = new BufferedReader(new FileReader(marriageFile));
			} else {
				marriageReader.reset();					//reset to mark of previous reading
//				System.out.printf("Reader was reset\n");
			}//if
			
			
			String line = marriageReader.readLine();
			marriageReader.mark(60);
			while (line != null) {
				
				String[] parts = line.split(";");							//the string array containing marriage info
				int year = Integer.parseInt(parts[INDEX_MARRIAGE_START]);	//the current year of parsing
				
				
				// check if current year has reached start year parameter
				if (year < from) {
					line = marriageReader.readLine();	//read new line
					continue;
					
				} else if (year <= to) {						//year is smaller than or equal to end year parameter
					pMarriages.add(parseMarriage(parts));		//parse marriage, returns ParseObject and adds it to list
					marriageReader.mark(60);
//					System.out.printf("Reader was marked\n");
					line = marriageReader.readLine();
					continue;
				} else {
					return pMarriages;
				}//if
									
			}//while
			
			marriageReader.close();			//newly read lines are null --> close stream
			marriageReader = null;
//			System.out.printf("Reader was closed\n");
			return null;
						
		} catch (IOException e) {
			e.getStackTrace();
			System.out.printf("oh no something went wrong: %s\n", e);
			return null;
			
		}//catch
		
	}//parseMarriages
	
	
	private MarriageObject parseMarriage(String[] parts) {
		String 	husbandName = parts[INDEX_NAME_HUSBAND];						//husband name
		String 	wifeName	= parts[INDEX_NAME_WIFE];							//wife name
		int		startYear	= Integer.parseInt(parts[INDEX_MARRIAGE_START]);	//marriage start
		int		endYear		= Integer.parseInt(parts[INDEX_MARRIAGE_END]);		//marriage end
		
		return new MarriageObject(husbandName, wifeName, startYear, endYear);
	}//parseMarriage

	
	
	
	public List<FamilyObject> parseFamilyInfo() {
		System.out.printf("Collecting family information...\n");
		List<FamilyObject> familyObjects = new ArrayList<FamilyObject>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(familiesFile));
			
			String line = br.readLine();		//read first line
			while (line != null) {
				
				if (line.contains(";")) {		// is document csv. format?
					// Parsing the family info //
					String parts[] = line.split(";");
					familyObjects.add( new FamilyObject(parts[INDEX_FAMILY_NAME], Integer.parseInt(parts[INDEX_POLITICAL_POWER]),
							Integer.parseInt(parts[INDEX_WEALTH]), Integer.parseInt(parts[INDEX_JOINED_ARSTCRCY])) );
				
				}//if
				line = br.readLine();
				
			}//while
			br.close();
			return familyObjects;
			
		} catch (IOException e) {
			e.getStackTrace();
			return null;
		}//catch

	}//parseFamilyInfo




	public void setMarriageFile(String marriageFile) {
		this.marriageFile = marriageFile;
		System.out.printf("Set marriage file path: %s\n", marriageFile);
	}//setMarriageFile
	
	
	public void setFamilyFile(String familyFile) {
		this.familiesFile = familyFile;
		System.out.printf("Set family file path: %s\n", familyFile);
	}//setFamilyFile




	public boolean isValidMarriageFile(String marriageFile) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(marriageFile));
			br.close();
			return true;
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}//try
		
	}//isValidMarriageFile
	
	
	public boolean isValidFamilyFile(String familyFile) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(familyFile));
			br.close();
			return true;
		} catch (FileNotFoundException e) {
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}//catch
		
	}//isValidFamilyFile


	
	
}//Parser
