package model;

import java.util.Observable;

import model.attributes.AbstractAttributeManager;
import model.attributes.FinalNumericAttribute;
import model.attributes.MarriageActiveAttribute;

/**
 * This class represents marriages. It extends from {@link AbstractAttributeManager} and holds attributes about the husband and wife,
 * the start and end date of the marriage and an attribute holding the state of the marriage. 
 * @author Marten Sigwart
 *
 */
public class Marriage extends AbstractAttributeManager {
	
	public static final String ATTRIBUTE_NAME_MARRIAGE 			= "Marriage";
	public static final String ATTRIBUTE_NAME_ID 				= "MarID";
	public static final String ATTRIBUTE_NAME_HUSBAND 			= "Husband";
	public static final String ATTRIBUTE_NAME_WIFE 				= "Wife";
	public static final String ATTRIBUTE_NAME_MARRIAGE_START 	= "StartOfMarriage";
	public static final String ATTRIBUTE_NAME_MARRIAGE_END 		= "EndOfMarriage";
	public static final String ATTRIBUTE_NAME_ACTIVE			= "Active";


	
	public 	static final int EVENT_MARRIAGE_STARTED	= 0;
	public 	static final int EVENT_MARRIAGE_ENDED	= 1;
	
	
	
	private static int marriageId = 0;
	
	
	/**
	 * Resets the static marriage ID counter
	 */
	public static void reset() {
		marriageId = 0;
	}//reset
	
	
	public Marriage(int id, Person husband, Person wife, int dateStartMarriage, int dateEndMarriage) {
		super(ATTRIBUTE_NAME_MARRIAGE);
		addAttribute(new FinalNumericAttribute<Integer>(ATTRIBUTE_NAME_ID, id));
		addAttribute(new FinalNumericAttribute<Integer>(ATTRIBUTE_NAME_MARRIAGE_START, dateStartMarriage));
		addAttribute(new FinalNumericAttribute<Integer>(ATTRIBUTE_NAME_MARRIAGE_END, dateEndMarriage));
		addAttribute(new MarriageActiveAttribute(ATTRIBUTE_NAME_ACTIVE, false, this));
		addAttribute(ATTRIBUTE_NAME_HUSBAND, husband);
		addAttribute(ATTRIBUTE_NAME_WIFE, wife);
		addObserver(husband);
		addObserver(wife);
		
		getHusband().setSpouse(wife);
		getWife().setSpouse(husband);
		
	}//Constructor
	
	
	public Marriage(int id, Person p) {
		this(id, p, p, -1, -1);
		
	}//Constructor
	
	
	public Marriage(Person husband, Person wife, int dateStartMarriage, int dateEndMarriage) {
		this(marriageId++, husband, wife, dateStartMarriage, dateEndMarriage);
		
	}//Constructor
	
	
	/**
	 * 'Activates' this {@code Marriage}. Notifies all its observers about event {@link #EVENT_MARRIAGE_STARTED}.
	 */
	public void startMarriage() {
		System.out.printf("Event Marriage STARTED\n");
		setChanged();
		notifyObservers(EVENT_MARRIAGE_STARTED);
		
	}//startMarriage
	
	
	/**
	 * 'Deactivates' this {@code Marriage}. Notifies all its observers about event {@link #EVENT_MARRIAGE_ENDED}.
	 */
	public void endMarriage() {
		System.out.printf("Event Marriage ENDED\n");
		setChanged();
		notifyObservers(EVENT_MARRIAGE_ENDED);
		
	}//endMarriage
	
	
	
	@Override
	public Integer getValue() {
		return getId();
	}//getValue
	
	public Integer getId() {
		return (Integer)getAttributeValue(ATTRIBUTE_NAME_ID);
	}//getId

	public int getStartOfMarriage() {
		return (Integer)getAttributeValue(ATTRIBUTE_NAME_MARRIAGE_START);
	}//getDateStartMarriage

	public int getEndOfMarriage() {
		return (Integer)getAttributeValue(ATTRIBUTE_NAME_MARRIAGE_END);
	}//getDateEndMarriage

	public Person getHusband() {
		return (Person)getAttribute(ATTRIBUTE_NAME_HUSBAND);
	}//getHusband
	
	public Person getWife() {
		return (Person)getAttribute(ATTRIBUTE_NAME_WIFE);
	}//getHusband
	
	public String getHusbandName() {
		return getHusband().getFamily().getName();
	}//getHusbandName
	
	public String getWifeName() {
		return getWife().getFamily().getName();
	}//getWifeName
	
	public Family getFamily() {
		return getHusband().getFamily();
	}//getFamily
	
	public boolean isMarriageActive() {
		return (Boolean)getAttributeValue(ATTRIBUTE_NAME_ACTIVE);
	}//getMarriageActive
	

	
	
	@Override
	public String[] getDefaultAttrLabels() {
		String[] mapping = {ATTRIBUTE_NAME_ID, /*ATTRIBUTE_NAME_HUSBAND+"."+Family.ATTRIBUTE_NAME_ID,*/ 
				ATTRIBUTE_NAME_HUSBAND+"."+Family.ATTRIBUTE_NAME_FAMILYNAME,
				ATTRIBUTE_NAME_WIFE+"."+Family.ATTRIBUTE_NAME_FAMILYNAME, 
				ATTRIBUTE_NAME_MARRIAGE_START, ATTRIBUTE_NAME_MARRIAGE_END, ATTRIBUTE_NAME_ACTIVE};
		return mapping;
	}//getDefaultAttributeMapping
	
	
	@Override
	public String[] getAllAttributeLabels() {
		String[] mapping = {ATTRIBUTE_NAME_ID, /*ATTRIBUTE_NAME_HUSBAND+"."+Family.ATTRIBUTE_NAME_ID,*/ 
				ATTRIBUTE_NAME_HUSBAND+"."+Family.ATTRIBUTE_NAME_FAMILYNAME,
				ATTRIBUTE_NAME_WIFE+"."+Family.ATTRIBUTE_NAME_FAMILYNAME, 
				ATTRIBUTE_NAME_MARRIAGE_START, ATTRIBUTE_NAME_MARRIAGE_END, ATTRIBUTE_NAME_ACTIVE};
		return mapping;
	}//getDefaultAttributeMapping
	
	
	public String[] getSimpleAttrLabels() {
		String[] attrLabels = {ATTRIBUTE_NAME_HUSBAND+"."+Person.ATTRIBUTE_NAME_ID,
				ATTRIBUTE_NAME_HUSBAND+"."+Family.ATTRIBUTE_NAME_FAMILYNAME, 
				ATTRIBUTE_NAME_WIFE+"."+Person.ATTRIBUTE_NAME_ID,
				ATTRIBUTE_NAME_WIFE+"."+Family.ATTRIBUTE_NAME_FAMILYNAME};
		return attrLabels;
	}//getSimpleAttrLabels

	
	
	public String getFormattedString() {
		return String.format("ID: %4d, Husband: %-20s, Wife: %-20s", getId(), getHusbandName(), getWifeName());
	}//getFormattedString

	public String getTableString() {
		return String.format("%4d |%5d | %-20s|%5d | %-20s", getId(), getHusband().getId(), getHusbandName(), getWife().getId(), getWifeName());
	}//getFormattedString


	@Override
	public void update(Observable o, Object arg) {
	}








	
}//Marriage
