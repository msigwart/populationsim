package model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.attributes.AbstractAttributeManager;



public class AttributeManagerTableModel<T extends AbstractAttributeManager> extends AbstractTableModel {

	private String[] columnNames = {};
	private List<T> elements;
	
	public AttributeManagerTableModel(String[] columnNames) {
		this.columnNames = columnNames;
		this.elements = new ArrayList<T>();
	}//Constructor
	
	@Override
	public int getRowCount() {
		return elements.size();
	}//getRowCount

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}//getColumnCount

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}//getColumnNames
	
	@Override
	public Object getValueAt(int rowIndex, int colIndex) {
		return elements.get(rowIndex).getAttributeValue(columnNames[colIndex]);
	}//getValueAt
	
	
	@Override
	public boolean isCellEditable(int row, int col) {		//make all cells not editable
		return false;
	}//isCellEditable
	
	@Override													//Return the right class for sorting
	public Class<?> getColumnClass(int col) {
		return getValueAt(0, col).getClass();
	}//getColumnClass
	
	
	/**
	 * 
	 * @return returns the underlying list object of the model
	 */
	public List<T> getElements() {
		return elements;
	}//getList
	
	public void addRow(T element) {
		elements.add(element);
	}//addRow
	
	public void addRow(Integer index, T element) {
		elements.add(index, element);
	}//addRow

	public boolean hasElement(T element) {
		return elements.contains(element);
	}//hasElement
	
	public T getElement(int index) {
		return elements.get(index);
	}//getFamily

	public void removeAll() {
		for (int i=elements.size()-1; i>=0; i--) {
			elements.remove(i);
			fireTableRowsDeleted(i, i);
		}//for
	}//removeAll

	public int size() {
		return elements.size();
	}//size


	
}//AttributeManagerTableModel
