package model.exporter;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


import model.Family;
import model.Marriage;
import model.ModelControl;
import model.Person;



public class Exporter {

	private ModelControl model;
	private List<AbstractExportFormat> formats;
	
	public Exporter(ModelControl mc) {
		model = mc;
		formats = new ArrayList<AbstractExportFormat>();
		formats.add(new FormatComparison(model));
		formats.add(new FormatMatlab(model));
	}//Constructor
	
	
	public List<AbstractExportFormat> getFormatList() {
		return formats;
	}//getFormatList
	

	
	
//############### OUTPUT METHODS ############################################################################	
	/**
	 * Writes the recent state of the socialnet into a textfile
	 * @param filename the filename
	 * @param hToW boolean whether to write the husbandToWife or the wifeToHusband matrix
	 * @throws FileNotFoundException 
	 */
	public void writeMatrixToFile(String filename, boolean hToW) {
		model.getSocialNet().writeMatrixToFile(filename, hToW);
		
	}//writeNetToFile

	/**
	 * Writes the family index with the corresponding family name to file
	 * @param filename the filename
	 * @throws FileNotFoundException
	 */
	public void writeFamilyIndexToFile(String filename) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(filename);
		for (Family f: model.getFamilies().getElements()) {
			pw.printf("%3d:  %s\n", f.getId(), f.getAttributeName());
		}//for
		pw.close();
	}//writeFamilyIndexToFile

	/**
	 * Exports all current marriages and possibilities to file
	 * @throws FileNotFoundException
	 */
	public void exportCurrentMarriages() throws FileNotFoundException {
		String filename 		= "export/marriages" + model.getCurrentYear();
		PrintWriter pw 			= new PrintWriter(filename);
		List<Person> husbands 	= new ArrayList<Person>();
		List<Person> wives 		= new ArrayList<Person>();
		
		pw.printf("Marriages of year %d\n", model.getCurrentYear());
		pw.printf(" #   | ID   | Husband             | ID   | Wife               \n" +
				  "-----+------+---------------------+------+--------------------\n");
		for (Marriage m: model.getAlmostMarried().getElements()) {
			husbands.add(m.getHusband());				//add husband and wife to lists for further processing
			wives.add(m.getWife());
			pw.printf("%s\n", m.getTableString());
		}//for
		
		pw.printf("\nPossible marriages: \n");
		for (Person h: husbands) {
			pw.printf("%s\n" +
					  "------+---------------------+--------+----+--------+-----------+-----------+---\n", h.getTableString());
			for (Person w: wives) {
				if (w == h.getSpouse()) {
					pw.printf("%s\n", w.getTableString(true));
				} else {
					pw.printf("%s\n", w.getTableString(false));
				}//if
			}//for
			pw.printf("\n");
		}//for
		
		pw.close();
	}//exportCurrentMarriages

	/**
	 * Exports all marriages to a specific file.
	 */
	public void exportAllMarriages(AbstractExportFormat format, String[] attrNames, boolean numValues) throws FileNotFoundException {
		PrintWriter  pw 	= new PrintWriter("export/"+format.getFileName());
		int originalYear 	= model.getCurrentYear();			//save current year to reset model after export
		Integer[] statYears = (Integer[])model.getYearsOfMarriages().toArray();
		
		format.exportMarriages(pw, statYears, attrNames, numValues);
		
		pw.close();										//close print writer
		model.setModelToYear(originalYear);				// reset to the year that it was before

	}//exportAllMarriages
	
	public void exportAllMarriages(String formatName, String[] attrNames, boolean numValues) throws FileNotFoundException {
		AbstractExportFormat f = getExportFormat(formatName);
		exportAllMarriages(f, attrNames, numValues);
	}//exportAllMarriages


	public boolean hasFormat(String format) {
		for (AbstractExportFormat f: formats) {
			if (format.equalsIgnoreCase(f.getName())) {
				return true;
			}//if
		}//for
		return false;
	}//hasFormat


	private AbstractExportFormat getExportFormat(String format) {
		for (AbstractExportFormat f: formats) {
			if (format.equalsIgnoreCase(f.getName())) {
				return f;
			}//if
		}//for
		return null;
	}//getFormat





	
	
	
	
	
	
	
}//Exporter
