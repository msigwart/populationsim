package model.exporter;

import java.io.PrintWriter;


import model.ModelControl;

public abstract class AbstractExportFormat {
	
	protected ModelControl 	model;
	private	  String	  	name;
	private   String		filename;
	
	public AbstractExportFormat(ModelControl model, String name, String filename) {
		this.model 	= model;
		this.name 	= name;
		this.filename = filename;
	}//AbstractExportFormat
	
	public String getName() {
		return name;
	}//getButton
	
	public String getFileName() {
		return filename;
	}//getFileName
	
	public abstract void exportMarriages(PrintWriter pw, Integer[] years, String[] attrNames, boolean numValues);
	
}//ExportFormat
