package model.exporter;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import model.Family;
import model.Marriage;
import model.ModelControl;
import model.Person;


public class FormatMatlab extends AbstractExportFormat {

	public FormatMatlab(ModelControl mc) {
		super(mc, "Matlab", "matlabMarriages.csv");
	}//Constructor
	
	@Override
	public void exportMarriages(PrintWriter pw, Integer[] years, String[] attrNames, boolean numValues) {
		
//		attrMapping[Family.INDEX_ID] = false;				//We don't want to export these attributes
//		attrMapping[Family.INDEX_NAME] = false;
		printHeader(pw, attrNames);
		
		int id = 0;
		//Print data
		for (int i=0; i<years.length; i++) {
			model.setModelToYear(years[i]);						// reset to first year of statistical years

			List<Person> husbands 	= new ArrayList<Person>();
			List<Person> wives 		= new ArrayList<Person>();
			
			for (Marriage m: model.getAlmostMarried().getElements()) {
				husbands.add(m.getHusband());				//add husband and wife to lists for further processing
				wives.add(m.getWife());
			}//for
			
			for (Person h: husbands) {
				for (Person w: wives) {
					int orgPartner = 0;
					if (w.getSpouse() == h) {
						orgPartner = 1;
					}//if
					pw.printf("%d", id++);							//print an ID
					printAttributeDeltas(pw, attrNames, h, w);	//print attributes h->w
					printAttributeDeltas(pw, attrNames, w, h);	//print attributes w->h
					pw.printf(";%d\n", orgPartner);					//print original marriage indicator
				}//for
			}//for
		
		}//for
	}//exportMarriages

	private void printAttributeDeltas(PrintWriter pw, String[] attrNames, Person p1, Person p2) {		//TODO make dynamic
		String attr = "";																				//TODO delta calculation could be part of attributes
		for (String s: attrNames) {			//iterate attributes
			switch (s) {
				case Family.ATTRIBUTE_NAME_PAGERANK_HTOW:
				case Family.ATTRIBUTE_NAME_PAGERANK_WTOH:
					BigDecimal delta = p1.getFamily().getPageRank(false).subtract(p2.getFamily().getPageRank(false));
					attr = attr + ";" + delta;
					break;
				case Family.ATTRIBUTE_NAME_POLITICS:
					int deltaP = p1.getFamily().getPolitics().getNumValue() - p2.getFamily().getPolitics().getNumValue();
					attr = attr + ";" + deltaP;
					break;
				case Family.ATTRIBUTE_NAME_WEALTH:
					int deltaW = p1.getFamily().getWealth().getNumValue() - p2.getFamily().getWealth().getNumValue();
					attr = attr + ";" + deltaW;
					break;
				default:
					break;
			}//switch
		}//for
		pw.print(attr);
	}//printAttributeDeltas

	private void printHeader(PrintWriter pw, String[] attrNames) {
		//Print header
		pw.printf("id");
		for (int j=0; j<2; j++) {
			for (int i=0; i<attrNames.length; i++) {			
				pw.printf(";Delta%s", attrNames[i]);
			}//for
		}//for

		pw.printf(";Married\n");
	}//printHeader

}//FormatMatlab
