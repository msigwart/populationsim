package model.exporter;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import model.Marriage;
import model.ModelControl;
import model.Person;

public class FormatComparison extends AbstractExportFormat {
	
	
	
	public FormatComparison(ModelControl mc) {
		super(mc, "Comparison", "allMarriages.csv");
	}//Constructor

	@Override
	public void exportMarriages(PrintWriter pw, Integer[] years, String[] attrNames, boolean numValues) {
		//Print header
		printHeader(pw, attrNames);
		
		//Print data
		for (int i=0; i<years.length; i++) {
			model.setModelToYear(years[i]);						// reset to first year of statistical years

			List<Person> husbands 	= new ArrayList<Person>();
			List<Person> wives 		= new ArrayList<Person>();
			
			for (Marriage m: model.getAlmostMarried().getElements()) {
				husbands.add(m.getHusband());				//add husband and wife to lists for further processing
				wives.add(m.getWife());
			}//for
			
			for (Person h: husbands) {
				for (Person w: wives) {
					int orgPartner = 0;
					if (w.getSpouse() == h) {
						orgPartner = 1;
					}//if
					pw.printf("%d;%d%s%s\n", model.getCurrentYear(), orgPartner,
							h.getFamily().getAttributeValueString(attrNames, numValues), w.getFamily().getAttributeValueString(attrNames, numValues));
				}//for
			}//for
		
		}//for
	}//exportMarriages

	private void printHeader(PrintWriter pw, String[] attrNames) {
		pw.printf("Year;Married");
		for (int j=0; j<2; j++) {
			for (int i=0; i<attrNames.length; i++) {
				pw.printf(";%s", attrNames[i]);
			}//for
		}//for
		pw.printf("\n");
	}//printHeader

}//FormatComparison
