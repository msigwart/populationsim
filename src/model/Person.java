package model;


import java.util.Observable;

import utility.ArrayUtils;

import model.attributes.AbstractAttributeManager;
import model.attributes.BooleanAttribute;
import model.attributes.EnumAttribute;
import model.attributes.FinalNumericAttribute;
import model.attributes.Gender;



public class Person extends AbstractAttributeManager {
		
	
	public static final String ATTRIBUTE_NAME_ID 			= "PerID";
	public static final String ATTRIBUTE_NAME_GENDER 		= "Gender";
	public static final String ATTRIBUTE_NAME_ORG_PARTNER 	= "org Partner";
	public static final String ATTRIBUTE_NAME_SPOUSE		= "Spouse";
	
	
	private static int persId = 0;
		

	
//	private Person	spouse;
//	private boolean orgPartner;				//boolean just for potential table display TODO find more elegant solution
		
	
	/**
	 * Resets the static person ID counter
	 */
	public static void reset() {
		persId = 0;
	}//reset
	
	
	
	@Override
	public String[] getDefaultAttrLabels() {
		String[] mapping = {ATTRIBUTE_NAME_ID, ATTRIBUTE_NAME_GENDER,
				Family.ATTRIBUTE_NAME_ID, Family.ATTRIBUTE_NAME_FAMILYNAME,
				Family.ATTRIBUTE_NAME_PAGERANK_HTOW, Family.ATTRIBUTE_NAME_PAGERANK_WTOH,
				Family.ATTRIBUTE_NAME_POLITICS, Family.ATTRIBUTE_NAME_WEALTH,
				ATTRIBUTE_NAME_ORG_PARTNER/*, ATTRIBUTE_NAME_SPOUSE*/};
		return mapping;
	}//getDefaultAttrMapping
	
	@Override
	public String[] getAllAttributeLabels() {
		String[] mapping = {ATTRIBUTE_NAME_ID, ATTRIBUTE_NAME_GENDER};
		mapping = ArrayUtils.concat(mapping, Family.ALL_ATTRIBUTE_NAMES);
		mapping = ArrayUtils.concat(mapping, new String[]{ATTRIBUTE_NAME_ORG_PARTNER, ATTRIBUTE_NAME_SPOUSE});
		return mapping;
	}//getAllAttributeLabels
	
	
//##### Constructors ################
	
	public Person(int id, Gender gender, Family family) {
		super("Person");
		addAttribute(new FinalNumericAttribute<Integer>(ATTRIBUTE_NAME_ID, id));
		addAttribute(new EnumAttribute<Gender>(ATTRIBUTE_NAME_GENDER, gender));
		addAttribute(family);
		addObserver(family);
		addAttribute(new BooleanAttribute(ATTRIBUTE_NAME_ORG_PARTNER, false));		//boolean just for potential table display TODO find more elegant solution
		addAttribute(ATTRIBUTE_NAME_SPOUSE, null);
		
	}//Constructor
	
	
	public Person(int id, Family family) {
		this(id, Gender.UNDEFINED, family);
	}//Constructor
	
	
	public Person(Gender gender, Family family) {
		this(persId++, gender, family);
	}//Constructor
	

	
	
//##### Getters & Setters ################
	
	@Override
	public Integer getValue() {
		return getId();
	}//getValue
	
	
	public Family getFamily() {
		return (Family)getAttribute(Family.ATTRIBUTE_NAME_FAMILY);
	}//getFamily

	public Integer getId() {
		return (Integer)getAttributeValue(ATTRIBUTE_NAME_ID);
	}//getId
	
	public Gender getGender() {
		return (Gender)getAttributeValue(ATTRIBUTE_NAME_GENDER);
	}//getGender
	
	public void setOrgPartner(boolean b) {
		((BooleanAttribute)getAttribute(ATTRIBUTE_NAME_ORG_PARTNER)).setValue(b);
	}//setOrgPartner
	
	public Person getSpouse() {
		return ((Person)getAttributeValue(ATTRIBUTE_NAME_SPOUSE));
	}//getSpouse

	public void setSpouse(Person spouse) {
		addAttribute(ATTRIBUTE_NAME_SPOUSE, spouse);
	}//setSpouse




	public String getTableString(boolean orgPartner) {
		if (orgPartner) {
			return getTableString() + "| X ";
		} else {
			return getTableString() + "|   ";
		}//if
	}//getTableString
	
	public String getTableString() {
		return String.format("%5d | %-20s| %-7s| %2d | %.4f | %-10s| %-10s", 
				getId(), getFamily().getAttributeName(), getGender(), getFamily().getNoOfConnections(), getFamily().getPageRank(true), 
				getFamily().getPolitics(), getFamily().getWealth());
	}//getTableString



	@Override
	public void update(Observable o, Object arg) {
		switch ((int)arg) {
			case Marriage.EVENT_MARRIAGE_STARTED:
				setChanged();
				notifyObservers(arg);
				break;
			case Marriage.EVENT_MARRIAGE_ENDED:
				setChanged();
				notifyObservers(arg);
				break;
			default:
				break;
		}//switch
	}//update


}//Person
